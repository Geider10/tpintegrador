﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 T[] UnityEngine.ProBuilder.KdTree.HyperRect`1::get_MinPoint()
// 0x00000002 System.Void UnityEngine.ProBuilder.KdTree.HyperRect`1::set_MinPoint(T[])
// 0x00000003 T[] UnityEngine.ProBuilder.KdTree.HyperRect`1::get_MaxPoint()
// 0x00000004 System.Void UnityEngine.ProBuilder.KdTree.HyperRect`1::set_MaxPoint(T[])
// 0x00000005 UnityEngine.ProBuilder.KdTree.HyperRect`1<T> UnityEngine.ProBuilder.KdTree.HyperRect`1::Infinite(System.Int32,UnityEngine.ProBuilder.KdTree.ITypeMath`1<T>)
// 0x00000006 T[] UnityEngine.ProBuilder.KdTree.HyperRect`1::GetClosestPoint(T[],UnityEngine.ProBuilder.KdTree.ITypeMath`1<T>)
// 0x00000007 UnityEngine.ProBuilder.KdTree.HyperRect`1<T> UnityEngine.ProBuilder.KdTree.HyperRect`1::Clone()
// 0x00000008 System.Void UnityEngine.ProBuilder.KdTree.DuplicateNodeError::.ctor()
extern void DuplicateNodeError__ctor_m6BBBD7828CF9D6E147675E8AC3B60E7175A7321A (void);
// 0x00000009 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2::.ctor(System.Int32,UnityEngine.ProBuilder.KdTree.ITypeMath`1<TKey>)
// 0x0000000A System.Void UnityEngine.ProBuilder.KdTree.KdTree`2::.ctor(System.Int32,UnityEngine.ProBuilder.KdTree.ITypeMath`1<TKey>,UnityEngine.ProBuilder.KdTree.AddDuplicateBehavior)
// 0x0000000B UnityEngine.ProBuilder.KdTree.AddDuplicateBehavior UnityEngine.ProBuilder.KdTree.KdTree`2::get_AddDuplicateBehavior()
// 0x0000000C System.Void UnityEngine.ProBuilder.KdTree.KdTree`2::set_AddDuplicateBehavior(UnityEngine.ProBuilder.KdTree.AddDuplicateBehavior)
// 0x0000000D System.Boolean UnityEngine.ProBuilder.KdTree.KdTree`2::Add(TKey[],TValue)
// 0x0000000E System.Void UnityEngine.ProBuilder.KdTree.KdTree`2::AddNearestNeighbours(UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>,TKey[],UnityEngine.ProBuilder.KdTree.HyperRect`1<TKey>,System.Int32,UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2<UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>,TKey>,TKey)
// 0x0000000F UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>[] UnityEngine.ProBuilder.KdTree.KdTree`2::RadialSearch(TKey[],TKey,System.Int32)
// 0x00000010 System.Int32 UnityEngine.ProBuilder.KdTree.KdTree`2::get_Count()
// 0x00000011 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2::set_Count(System.Int32)
// 0x00000012 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2::AddNodeToStringBuilder(UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>,System.Text.StringBuilder,System.Int32)
// 0x00000013 System.String UnityEngine.ProBuilder.KdTree.KdTree`2::ToString()
// 0x00000014 System.Collections.Generic.IEnumerator`1<UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>> UnityEngine.ProBuilder.KdTree.KdTree`2::GetEnumerator()
// 0x00000015 System.Collections.IEnumerator UnityEngine.ProBuilder.KdTree.KdTree`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000016 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2_<>c__DisplayClass33_0::.ctor()
// 0x00000017 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2_<>c__DisplayClass33_0::<GetEnumerator>b__0(UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>)
// 0x00000018 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2_<>c__DisplayClass33_0::<GetEnumerator>b__1(UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>)
// 0x00000019 System.Void UnityEngine.ProBuilder.KdTree.KdTree`2_<GetEnumerator>d__33::.ctor(System.Int32)
// 0x0000001A System.Void UnityEngine.ProBuilder.KdTree.KdTree`2_<GetEnumerator>d__33::System.IDisposable.Dispose()
// 0x0000001B System.Boolean UnityEngine.ProBuilder.KdTree.KdTree`2_<GetEnumerator>d__33::MoveNext()
// 0x0000001C UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue> UnityEngine.ProBuilder.KdTree.KdTree`2_<GetEnumerator>d__33::System.Collections.Generic.IEnumerator<UnityEngine.ProBuilder.KdTree.KdTreeNode<TKey,TValue>>.get_Current()
// 0x0000001D System.Void UnityEngine.ProBuilder.KdTree.KdTree`2_<GetEnumerator>d__33::System.Collections.IEnumerator.Reset()
// 0x0000001E System.Object UnityEngine.ProBuilder.KdTree.KdTree`2_<GetEnumerator>d__33::System.Collections.IEnumerator.get_Current()
// 0x0000001F System.Void UnityEngine.ProBuilder.KdTree.KdTreeNode`2::.ctor()
// 0x00000020 System.Void UnityEngine.ProBuilder.KdTree.KdTreeNode`2::.ctor(TKey[],TValue)
// 0x00000021 UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue> UnityEngine.ProBuilder.KdTree.KdTreeNode`2::get_Item(System.Int32)
// 0x00000022 System.Void UnityEngine.ProBuilder.KdTree.KdTreeNode`2::set_Item(System.Int32,UnityEngine.ProBuilder.KdTree.KdTreeNode`2<TKey,TValue>)
// 0x00000023 System.Void UnityEngine.ProBuilder.KdTree.KdTreeNode`2::AddDuplicate(TValue)
// 0x00000024 System.String UnityEngine.ProBuilder.KdTree.KdTreeNode`2::ToString()
// 0x00000025 System.Int32 UnityEngine.ProBuilder.KdTree.ITypeMath`1::Compare(T,T)
// 0x00000026 T UnityEngine.ProBuilder.KdTree.ITypeMath`1::get_MinValue()
// 0x00000027 System.Boolean UnityEngine.ProBuilder.KdTree.ITypeMath`1::AreEqual(T[],T[])
// 0x00000028 T UnityEngine.ProBuilder.KdTree.ITypeMath`1::Multiply(T,T)
// 0x00000029 T UnityEngine.ProBuilder.KdTree.ITypeMath`1::get_NegativeInfinity()
// 0x0000002A T UnityEngine.ProBuilder.KdTree.ITypeMath`1::get_PositiveInfinity()
// 0x0000002B T UnityEngine.ProBuilder.KdTree.ITypeMath`1::DistanceSquaredBetweenPoints(T[],T[])
// 0x0000002C System.Void UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::.ctor(System.Int32,UnityEngine.ProBuilder.KdTree.ITypeMath`1<TDistance>)
// 0x0000002D System.Int32 UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::get_MaxCapacity()
// 0x0000002E System.Int32 UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::get_Count()
// 0x0000002F System.Boolean UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::Add(TItem,TDistance)
// 0x00000030 TDistance UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::GetFurtherestDistance()
// 0x00000031 TItem UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::RemoveFurtherest()
// 0x00000032 System.Boolean UnityEngine.ProBuilder.KdTree.NearestNeighbourList`2::get_IsCapacityReached()
// 0x00000033 System.Void UnityEngine.ProBuilder.KdTree.PriorityQueue`2::.ctor(System.Int32,UnityEngine.ProBuilder.KdTree.ITypeMath`1<TPriority>)
// 0x00000034 System.Int32 UnityEngine.ProBuilder.KdTree.PriorityQueue`2::get_Count()
// 0x00000035 System.Void UnityEngine.ProBuilder.KdTree.PriorityQueue`2::ExpandCapacity()
// 0x00000036 System.Void UnityEngine.ProBuilder.KdTree.PriorityQueue`2::Enqueue(TItem,TPriority)
// 0x00000037 TItem UnityEngine.ProBuilder.KdTree.PriorityQueue`2::Dequeue()
// 0x00000038 System.Void UnityEngine.ProBuilder.KdTree.PriorityQueue`2::ReorderItem(System.Int32,System.Int32)
// 0x00000039 TPriority UnityEngine.ProBuilder.KdTree.PriorityQueue`2::GetHighestPriority()
// 0x0000003A System.Int32 UnityEngine.ProBuilder.KdTree.Math.FloatMath::Compare(System.Single,System.Single)
extern void FloatMath_Compare_mC02579C119B46E3960E90DC2F07CF6705021165C (void);
// 0x0000003B System.Boolean UnityEngine.ProBuilder.KdTree.Math.FloatMath::AreEqual(System.Single,System.Single)
extern void FloatMath_AreEqual_mA4D17F4CD1F68966F4AD65162F48898719D71072 (void);
// 0x0000003C System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::get_MinValue()
extern void FloatMath_get_MinValue_m63C7634494C57290127D6A87433F8E5E2AF299C1 (void);
// 0x0000003D System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::get_Zero()
extern void FloatMath_get_Zero_mF6119E75CDD90BC14E5B66E3785DA418E8BC7197 (void);
// 0x0000003E System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::get_NegativeInfinity()
extern void FloatMath_get_NegativeInfinity_m15A53DFCB5324C84BE031D1A06AA74ADE073E3BD (void);
// 0x0000003F System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::get_PositiveInfinity()
extern void FloatMath_get_PositiveInfinity_m5C2CCC1449EBA886D6E24AF19FD2D5F795400D63 (void);
// 0x00000040 System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::Add(System.Single,System.Single)
extern void FloatMath_Add_mCDB89A480B79B7ECA21072A72B81D71B3A5895E3 (void);
// 0x00000041 System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::Subtract(System.Single,System.Single)
extern void FloatMath_Subtract_mDE3E17327848BDF08E79A2DBBC30299A734D1977 (void);
// 0x00000042 System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::Multiply(System.Single,System.Single)
extern void FloatMath_Multiply_m4DAC5E4680E0B8E9D8281FBDC82826C5FB876C38 (void);
// 0x00000043 System.Single UnityEngine.ProBuilder.KdTree.Math.FloatMath::DistanceSquaredBetweenPoints(System.Single[],System.Single[])
extern void FloatMath_DistanceSquaredBetweenPoints_m91F287DB2EC7D1F9A0ED8899EE110EC3BD78EC82 (void);
// 0x00000044 System.Void UnityEngine.ProBuilder.KdTree.Math.FloatMath::.ctor()
extern void FloatMath__ctor_mC9B6EB3780AB7CBBA318577DB6F6BA8AF215C9D4 (void);
// 0x00000045 System.Int32 UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::Compare(T,T)
// 0x00000046 System.Boolean UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::AreEqual(T,T)
// 0x00000047 System.Boolean UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::AreEqual(T[],T[])
// 0x00000048 T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::get_MinValue()
// 0x00000049 T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::get_Zero()
// 0x0000004A T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::get_NegativeInfinity()
// 0x0000004B T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::get_PositiveInfinity()
// 0x0000004C T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::Add(T,T)
// 0x0000004D T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::Subtract(T,T)
// 0x0000004E T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::Multiply(T,T)
// 0x0000004F T UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::DistanceSquaredBetweenPoints(T[],T[])
// 0x00000050 System.Void UnityEngine.ProBuilder.KdTree.Math.TypeMath`1::.ctor()
static Il2CppMethodPointer s_methodPointers[80] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DuplicateNodeError__ctor_m6BBBD7828CF9D6E147675E8AC3B60E7175A7321A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FloatMath_Compare_mC02579C119B46E3960E90DC2F07CF6705021165C,
	FloatMath_AreEqual_mA4D17F4CD1F68966F4AD65162F48898719D71072,
	FloatMath_get_MinValue_m63C7634494C57290127D6A87433F8E5E2AF299C1,
	FloatMath_get_Zero_mF6119E75CDD90BC14E5B66E3785DA418E8BC7197,
	FloatMath_get_NegativeInfinity_m15A53DFCB5324C84BE031D1A06AA74ADE073E3BD,
	FloatMath_get_PositiveInfinity_m5C2CCC1449EBA886D6E24AF19FD2D5F795400D63,
	FloatMath_Add_mCDB89A480B79B7ECA21072A72B81D71B3A5895E3,
	FloatMath_Subtract_mDE3E17327848BDF08E79A2DBBC30299A734D1977,
	FloatMath_Multiply_m4DAC5E4680E0B8E9D8281FBDC82826C5FB876C38,
	FloatMath_DistanceSquaredBetweenPoints_m91F287DB2EC7D1F9A0ED8899EE110EC3BD78EC82,
	FloatMath__ctor_mC9B6EB3780AB7CBBA318577DB6F6BA8AF215C9D4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[80] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1103,
	1910,
	686,
	686,
	686,
	686,
	1911,
	1911,
	1911,
	1912,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000002, { 0, 6 } },
	{ 0x02000005, { 6, 30 } },
	{ 0x02000006, { 36, 1 } },
	{ 0x02000007, { 37, 11 } },
	{ 0x02000008, { 48, 5 } },
	{ 0x0200000A, { 53, 9 } },
	{ 0x0200000C, { 62, 4 } },
	{ 0x0200000E, { 66, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[67] = 
{
	{ (Il2CppRGCTXDataType)2, 23572 },
	{ (Il2CppRGCTXDataType)3, 24951 },
	{ (Il2CppRGCTXDataType)3, 24952 },
	{ (Il2CppRGCTXDataType)3, 24953 },
	{ (Il2CppRGCTXDataType)2, 23574 },
	{ (Il2CppRGCTXDataType)3, 24954 },
	{ (Il2CppRGCTXDataType)3, 24955 },
	{ (Il2CppRGCTXDataType)3, 24956 },
	{ (Il2CppRGCTXDataType)3, 24957 },
	{ (Il2CppRGCTXDataType)2, 23588 },
	{ (Il2CppRGCTXDataType)3, 24958 },
	{ (Il2CppRGCTXDataType)2, 23584 },
	{ (Il2CppRGCTXDataType)3, 24959 },
	{ (Il2CppRGCTXDataType)3, 24960 },
	{ (Il2CppRGCTXDataType)3, 24961 },
	{ (Il2CppRGCTXDataType)3, 24962 },
	{ (Il2CppRGCTXDataType)3, 24963 },
	{ (Il2CppRGCTXDataType)3, 24964 },
	{ (Il2CppRGCTXDataType)3, 24965 },
	{ (Il2CppRGCTXDataType)3, 24966 },
	{ (Il2CppRGCTXDataType)3, 24967 },
	{ (Il2CppRGCTXDataType)3, 24968 },
	{ (Il2CppRGCTXDataType)3, 24969 },
	{ (Il2CppRGCTXDataType)3, 24970 },
	{ (Il2CppRGCTXDataType)3, 24971 },
	{ (Il2CppRGCTXDataType)2, 23590 },
	{ (Il2CppRGCTXDataType)3, 24972 },
	{ (Il2CppRGCTXDataType)3, 24973 },
	{ (Il2CppRGCTXDataType)2, 23589 },
	{ (Il2CppRGCTXDataType)3, 24974 },
	{ (Il2CppRGCTXDataType)2, 23591 },
	{ (Il2CppRGCTXDataType)3, 24975 },
	{ (Il2CppRGCTXDataType)3, 24976 },
	{ (Il2CppRGCTXDataType)2, 26340 },
	{ (Il2CppRGCTXDataType)3, 24977 },
	{ (Il2CppRGCTXDataType)3, 24978 },
	{ (Il2CppRGCTXDataType)3, 24979 },
	{ (Il2CppRGCTXDataType)2, 26341 },
	{ (Il2CppRGCTXDataType)3, 24980 },
	{ (Il2CppRGCTXDataType)2, 26342 },
	{ (Il2CppRGCTXDataType)3, 24981 },
	{ (Il2CppRGCTXDataType)3, 24982 },
	{ (Il2CppRGCTXDataType)2, 26343 },
	{ (Il2CppRGCTXDataType)3, 24983 },
	{ (Il2CppRGCTXDataType)3, 24984 },
	{ (Il2CppRGCTXDataType)3, 24985 },
	{ (Il2CppRGCTXDataType)3, 24986 },
	{ (Il2CppRGCTXDataType)3, 24987 },
	{ (Il2CppRGCTXDataType)2, 26344 },
	{ (Il2CppRGCTXDataType)3, 24988 },
	{ (Il2CppRGCTXDataType)3, 24989 },
	{ (Il2CppRGCTXDataType)2, 23616 },
	{ (Il2CppRGCTXDataType)2, 23617 },
	{ (Il2CppRGCTXDataType)2, 26345 },
	{ (Il2CppRGCTXDataType)3, 24990 },
	{ (Il2CppRGCTXDataType)3, 24991 },
	{ (Il2CppRGCTXDataType)3, 24992 },
	{ (Il2CppRGCTXDataType)2, 23629 },
	{ (Il2CppRGCTXDataType)3, 24993 },
	{ (Il2CppRGCTXDataType)3, 24994 },
	{ (Il2CppRGCTXDataType)3, 24995 },
	{ (Il2CppRGCTXDataType)3, 24996 },
	{ (Il2CppRGCTXDataType)2, 26346 },
	{ (Il2CppRGCTXDataType)3, 24997 },
	{ (Il2CppRGCTXDataType)3, 24998 },
	{ (Il2CppRGCTXDataType)2, 23640 },
	{ (Il2CppRGCTXDataType)3, 24999 },
};
extern const Il2CppCodeGenModule g_Unity_ProBuilder_KdTreeCodeGenModule;
const Il2CppCodeGenModule g_Unity_ProBuilder_KdTreeCodeGenModule = 
{
	"Unity.ProBuilder.KdTree.dll",
	80,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	67,
	s_rgctxValues,
	NULL,
};
