﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    private float correrVelocidad = 7.0f ;
   // private float mantenerRapidez = 4.0f;

    private Rigidbody rb;
    private int vida;
    public Camera camaraPrimeraPersona;

    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;

    GameObject piso1;//llamar internamente
    GameObject piso2;
    GameObject puerta1;
    GameObject piso06;//elimar GameObject
    GameObject piso07;
    GameObject proyectil;
    GameObject trampa01;
    GameObject puerta04;
    GameObject puerta06;
    //GameObject puerta07;
    //GameObject tarjetaacceso03;
    GameObject piso14;

    public RotarPuerta rotapuertascript;
    public Text vidaJugador;
    //public Text gameOver;
    public Text ganador;
    public Text armadura;
    public Text puntaje;

    private int chaleco;
    private int vidaRandom;
    public int puntos;
    //public float balaRandom;
    //private bool posicion;
    public  string nuevotext;
    public Text mensajeUI;
    public GestorPersistencia gestordatos;

    //escopeta
    private int armaRandom;
    GameObject balaescopeta01;
    GameObject balaescopeta02;
    GameObject balaescopeta03;

    GameObject cuchillo;
    //GameObject tarjetaacceso01;
    public string textGuardar;//pulir para que sea private
    public Text guardarDatosTxt;
    GameObject puerta08;
    GameObject puerta09;
    GameObject mensaje02ui;

    GameObject puerta04B;
    GameObject carcel01;
    GameObject carcel02;
    GameObject carcel03;
    GameObject carcel04;
    //guardar posicion 
    public  float posX;
    public  float posY;
    public  float posZ;
    public Vector3 Posicion;
    void Awake()
    {
        piso1 = GameObject.Find("Piso01");
        piso2 = GameObject.Find("Piso02");
        puerta1 = GameObject.Find("Puerta01");
        piso06 = GameObject.Find("Piso06");//lo va a buscar por su nombre en Unity
        piso07 = GameObject.Find("Piso07");
        proyectil = GameObject.Find("Proyectil");
        trampa01 = GameObject.Find("Puerta03");
        puerta04 = GameObject.Find("Puerta04");
        puerta06 = GameObject.Find("Puerta06");
       // puerta07 = GameObject.Find("Puerta07");
        //tarjetaacceso03 = GameObject.Find("TarjetaAcceso03");
        piso14 = GameObject.Find("Piso16");

        balaescopeta01 = GameObject.Find("BalaEscopeta01");
        balaescopeta02 = GameObject.Find("BalaEscopeta02");
        balaescopeta03 = GameObject.Find("BalaEscopeta03");
        cuchillo = GameObject.Find("Cuchillo");
        //tarjetaacceso01 = GameObject.Find("TarjetaAcceso01");
        puerta08 = GameObject.Find("Puerta08");
        puerta09 = GameObject.Find("Puerta09");
        mensaje02ui = GameObject.Find("Mensaje02UI");
        Cursor.lockState = CursorLockMode.Locked;
        puerta04B = GameObject.Find("Puerta04B");
        carcel01 = GameObject.Find("Carcel01");
        carcel02 = GameObject.Find("Carcel02");
        carcel03 = GameObject.Find("Carcel03");
        carcel04 = GameObject.Find("Carcel04");
    }

    void Start()//asignar variable antes que se ejecute el juego
    {
        //cargarDatos();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        //sonido1 = GetComponent<AudioSource>();
        //Cursor.lockState = CursorLockMode.Locked;
        vida = 200;
        vidaJugador.text = "HP: " + vida.ToString();
        //gameOver.enabled = false;
        ganador.enabled = false;
        SetearTextos();

        //tarjetaacceso03.SetActive(false);

        vidaRandom = Random.Range(20,31);//pasar dos valores 
        //balaRandom = Random.Range(10f, 31f);
        Debug.Log("jeringaVida: "+vidaRandom);
        trampa01.SetActive(false);//iniciazar Object Puerta03 "General" desactivo
        //posicion = false;
        chaleco = 100;
        armadura.text = "CHALECO: " + chaleco.ToString();
        armadura.enabled = false;

        puntos = 0;
        puntaje.text = "PUNTOS: " + puntos.ToString();
        
        ////se ejecuta siempre si se guarda la llave //se reconoce una se cargan todos los datos guardados
        //if (PlayerPrefs.HasKey("vida")&&PlayerPrefs.HasKey("chaleco10")&&PlayerPrefs.HasKey("puntajekill"))
        //{
        //    armadura.enabled = true;//activa el txt 
        //   // cargarDatos();
        //}
        //else if (PlayerPrefs.HasKey("vida"))
        //{
        //    vida = PlayerPrefs.GetInt("vida", vida);
        //    SetearTextos();
        //    Debug.Log("se cargo vida");
        //}
        mensajeUI.enabled = false;
        //nuevotext = "Hay 1 tarjeta escondida dentro del placard, pero ten cuidado";
        mensajeUI.text = nuevotext;

        armaRandom = Random.Range(1,10);
        Debug.Log("EL ARMA ES: " + armaRandom);
        //tarjetaacceso01.SetActive(false); inicia desactivada

        guardarDatosTxt.enabled = false;
        guardarDatosTxt.text = textGuardar;
        puerta09.SetActive(false);

        //transform.position = new Vector3(posX, posY, posZ);
    }
    //void OnGUI()
    //{
    //    GUI.TextField(new Rect(100, 200, 500, 100), "Hay 1 tarjeta escondida dentro del placard, pero ten cuidado");
    //}
    public void SetearTextos()
    {
        vidaJugador.text = "HP: " + vida.ToString();
        if (vida <= 0)
        {
            SceneManager.LoadScene("GameOver");//llamar directo a dicha escena para volver al MENU
        }
        armadura.text = "CHALECO: " + chaleco.ToString();
        if (chaleco <= 0)
        {
            armadura.enabled = false;
        }
        puntaje.text = "PUNTOS: " + puntos.ToString();
    }

    void Update()
    {
        movimientoPersonaje();
        
        switch (armaRandom)//probar el juego con distintas armas
        {
            case 1:
                Pistola();
                Debug.Log("El arma random es la PISTOLA"); 
                break;
            case 2:
                Escopeta();
                Debug.Log("El arma random es la ESCOPETA");
                break;
            case 3:
                ArmaCuchillo();
                Debug.Log("El arma ramdon es un CUCHILLO");
                break;
            case 4://aumentar la canche que no salga la misma arma
                Pistola();
                Debug.Log("El arma random es la PISTOLA");
                break;
            case 5:
                Escopeta();
                Debug.Log("El arma random es la ESCOPETA");
                break;
            case 6:
                ArmaCuchillo();
                Debug.Log("El arma ramdon es un CUCHILLO");
                break;
            case 7:
                Pistola();
                Debug.Log("El arma random es la PISTOLA");
                break;
            case 8:
                Escopeta();
                Debug.Log("El arma random es la ESCOPETA");
                break;
            case 9:
                ArmaCuchillo();
                Debug.Log("El arma ramdon es un CUCHILLO");
                break;
            default:
                Debug.Log("EL VALOR INGRESADO NO COINCIDE CON LAS OPCIONES");
                break;


        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
            Debug.Log("Presiono esc");
        }
      
        
        if (Input.GetKeyDown(KeyCode.C))//si presiona el efecto durra  7 segundos
        {
            rapidezDesplazamiento = correrVelocidad;
            Debug.Log("presiono c para correr");
            StartCoroutine(temporizador(7));//llamar y asignar el tiempo de regreso
        }


        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }

        if (transform.position.y <= -20)
        {
            //gameOver.enabled = true;
            SceneManager.LoadScene("GameOver");
        }

        //if (Input.GetKeyDown(KeyCode.R))//
        //{
        //    PlayerPrefs.DeleteKey("vida");//se reinician todas la llaves en general.
        //    PlayerPrefs.DeleteKey("chaleco10");
        //    PlayerPrefs.DeleteKey("punatajekill");
        //    SetearTextos();
        //    Debug.Log("presiono R, cuando carga el juego se reinicia vida y puntos, off armadura");

        //}
        //mensajeUI.text = nuevotext;

        //se ejecuta siempre si se guarda la llave //se reconoce una se cargan todos los datos guardados
        if (PlayerPrefs.HasKey("vida") && PlayerPrefs.HasKey("chaleco10") && PlayerPrefs.HasKey("puntajekill") && Input.GetKeyDown(KeyCode.T))
        {
            armadura.enabled = true;
            mensaje02ui.SetActive(false);
            cargarDatos();
            //GestorPersistencia.instancia.CargarDataPersistencia();
        }
        //else if(PlayerPrefs.HasKey("vida") && PlayerPrefs.HasKey("puntajekill") && Input.GetKeyDown(KeyCode.T))
        //{
        //    cargarDatos();
        //    GestorPersistencia.instancia.CargarDataPersistencia();
        //}
        if (Input.GetKeyDown(KeyCode.B))
        {
            vida -= 20;
            SetearTextos();
        }
    }
    private void Pistola()
    {

        if (Input.GetMouseButtonDown(0))//click izquierdo
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));//posicion del rayo sobre la pantalla

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);//mover segun la posicion del Jugador

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 18, ForceMode.Impulse);//define la distancia

            Destroy(pro, 1);


            GestorDeAudio.instancia.ReproducirSonido("sonidopistolasonidodisparosV06Música");

        }
    }
    private void Escopeta()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //bala01Escopeta
            Ray ray1 = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro1;
            pro1 = Instantiate(balaescopeta01, ray1.origin, transform.rotation);

            Rigidbody rb1 = pro1.GetComponent<Rigidbody>();
            rb1.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro1, 2);
            //bala02Escopeta
            Ray ray2 = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.4f, 0.6f, 0));

            GameObject pro2;
            pro2 = Instantiate(balaescopeta02, ray2.origin, transform.rotation);

            Rigidbody rb2 = pro2.GetComponent<Rigidbody>();
            rb2.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro2, 2);
            //bala03Escopeta
            Ray ray3 = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.6f, 0.6f, 0));

            GameObject pro3;
            pro3 = Instantiate(balaescopeta03, ray3.origin, transform.rotation);

            Rigidbody rb3 = pro3.GetComponent<Rigidbody>();
            rb3.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro3, 2);

            GestorDeAudio.instancia.ReproducirSonido("sonidoescopetaV03");
        }
    }
    private void ArmaCuchillo()
    {
        if (Input.GetMouseButtonDown(0))//click izquierdo
        {
            Ray ray4 = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));//posicion del rayo sobre la pantalla

            GameObject pro4;
            pro4= Instantiate(cuchillo, ray4.origin,transform.rotation);//mover segun la posicion del Jugador

            Rigidbody rb4 = pro4.GetComponent<Rigidbody>();
            rb4.AddForce(camaraPrimeraPersona.transform.forward * 21, ForceMode.Impulse);

            Destroy(pro4, 3);

            GestorDeAudio.instancia.ReproducirSonido("sonidocuchilloV01");
        }

    }
    private void movimientoPersonaje()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        //llamar al gestorDeAudio para reproducir PASOS.
    
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    private void OnTriggerStay(Collider collison)//collider dinamico
    {
        if (collison.gameObject.CompareTag("masvida") && Input.GetKeyDown(KeyCode.E))
        {//se ejecuta si presiona p y collisiona
            float vidaDiferencia;
            vidaDiferencia = 200 - vidaRandom;
            if (vida <= vidaDiferencia)
            {
                vida += vidaRandom;
                SetearTextos();//guardar los cambios en el canvas
                collison.gameObject.SetActive(false);
            }
            else
            {
                vida = 200;
                SetearTextos();//guardar los cambios en el canvas
                collison.gameObject.SetActive(false);
            }
            
            //Debug.Log("presiono la P");
        }
        if(collison.gameObject.CompareTag("botequinvida")&& Input.GetKeyDown(KeyCode.E))
        {
            if(vida <= 130)
            {
                vida += 70;
                SetearTextos();
                collison.gameObject.SetActive(false);
            }
            else
            {
                vida = 200;
                SetearTextos();
                collison.gameObject.SetActive(false);
            }
        }
     
        if (collison.gameObject.CompareTag("armadura") && Input.GetKeyDown(KeyCode.E))
        {
            armadura.enabled = true;//activa el txt, por defecto siempre es de 100 es escudo
            collison.gameObject.SetActive(false);
            //SetearTextos();no se acutualiza el canvas en el momneto
        }
    }
    private void OnCollisionEnter(Collision objetos)//collider estatico
    {
        //esta vez tiene rigidbody y se fresee toda su position
        if(objetos.gameObject.CompareTag("piso06"))
        {
            Destroy(piso06, 8.0f);//elminar GameObject por su nombre en X tiempo.
        }
        if (objetos.gameObject.CompareTag("piso07"))
        {
            Destroy(piso07, 8.0f);
        }
        if (objetos.gameObject.CompareTag("enemigo1"))
        {
            if (armadura.enabled==true)//se ejecuta si esta activo el txt
            {
                chaleco -= 15;
                SetearTextos();
            }
            else if (vida > 0)//segundo
            {
                vida -= 30;
                SetearTextos();
            }
        }
        if (objetos.gameObject.CompareTag("enemigo2"))//aplica para enemigo2 prifab o sin ser
        {
            //primero pregunta si esta activo el TXT ARAMADURA para descontarle primero a variable "CHALECO"
            //en el caso opuesto si no esta activo ARAMADURA le descuenta directo a la variable "VIDA"
            if (armadura.enabled==true)
            {
                chaleco -= 10;
                SetearTextos();
               // Debug.Log("hiso collision pirmero con chaleco");
            }
            else if (vida > 0)
                {
                    vida -= 20;
                    SetearTextos();
               // Debug.Log("hiso collision segunda con vida");
            }
            
        }
        if (objetos.gameObject.CompareTag("trampa01"))
        {
            trampa01.SetActive(true);//activar ese Object Pared03 para encerrarse
            objetos.gameObject.SetActive(false);//desaparece la trampa01

        }
        if (objetos.gameObject.CompareTag("rayoalien"))
        {
            //if (armadura.enabled==true)//el chaleco no tiene efecto sobre los rayoALIEN
            //{
            //    chaleco -= 5;
            //    SetearTextos();
            //}
            if (vida > 0)//segundo
            {
                vida -= 10;
                SetearTextos();
            }
        }
        if (objetos.gameObject.CompareTag("enemigo3"))
        {
            if (armadura.enabled==true)
            {
                chaleco -= 8;
                SetearTextos();
                Debug.Log("hiso collision primero con el chaleco");
            }
            else if (vida > 0)
            {
                vida -= 15;
                SetearTextos();
                Debug.Log("hiso segunda collision con vida");
            }
            //if (transform.position.z >= -2) vuelva a su posicion
            //{
            //    posicion = true;
            //}

            //transform.position.x=94;
        }
        if (objetos.gameObject.CompareTag("enemigo4"))
        {
            if (armadura.enabled==true)
            {
                chaleco -= 3;
                SetearTextos();
            }
            else if (vida > 0)
            {
                vida -= 5;
                SetearTextos();
            }
        }
        if (objetos.gameObject.CompareTag("trampa02"))
        {
            puerta06.SetActive(false);//desactivar Puerta06 para pasar
            objetos.gameObject.SetActive(false);

            carcel01.SetActive(false);
            carcel02.SetActive(false);
            carcel03.SetActive(false);
            carcel04.SetActive(false);
        }
        if (objetos.gameObject.CompareTag("trampa03"))
        {
            puerta06.SetActive(true);//activar Object
            objetos.gameObject.SetActive(false);

           
        }
        if (objetos.gameObject.CompareTag("pisoacido"))//le saca daño mas cuando rebota
        {
            vida -= 3;//el chaleco no tiene efecto
            SetearTextos();
            GestorDeAudio.instancia.ReproducirSonido("pisoacidoV01Música");
        }
        if (objetos.gameObject.CompareTag("enemigo5"))
        {
            if (armadura.enabled==true)//primero
            {
                chaleco -= 10;
                SetearTextos();
            }
            else if (vida > 0)//segundo
            {
                vida -= 20;
                SetearTextos();
            }
        }
        if (objetos.gameObject.CompareTag("trampa04"))
        {
            ganador.enabled = true;
            armadura.enabled = false;
            vidaJugador.enabled = false;
            puntaje.enabled = false;
            objetos.gameObject.SetActive(false);
            //if (transform.position.y <= -21)//elimar los txt de GameOver si se cumple la condicion 
            //{
            //    gameOver.enabled = false;
            //}
            //if (vida <= 0)//chequear
            //{
            //    gameOver.enabled = false;
            //}
        }
        if (objetos.gameObject.CompareTag("presidente"))//clon trampa del presidente
        {
            Destroy(piso14);
            objetos.gameObject.SetActive(false);
        }
        if (objetos.gameObject.CompareTag("mensaje01"))
        {
            mensajeUI.enabled = true;
            objetos.gameObject.SetActive(false);
            Destroy(mensajeUI, 5.0f);

        }
        if (objetos.gameObject.CompareTag("mensaje02"))
        {
            guardarDatosTxt.enabled = true;
            objetos.gameObject.SetActive(false);
            Destroy(guardarDatosTxt, 5.0f);
        }
        if (objetos.gameObject.CompareTag("trampa05"))
        {
            objetos.gameObject.SetActive(false);
            puerta08.SetActive(false);
            puerta09.SetActive(true);
        }
        if (objetos.gameObject.CompareTag("enemigo6"))
        {
            if (armadura.enabled == true)//primero
            {
                chaleco -= 8;
                SetearTextos();
            }
            else if (vida > 0)//segundo
            {
                vida -= 15;
                SetearTextos();
            }
        }
    }
    //se va a ejecutar en cada m/s//ocasiones respuesta inmediata
    private void OnCollisionStay(Collision preciso)
    {
        //cuenta con rigidbody pero no tigger ni is kinectic//tiene frezeado position (x,z)
        if (preciso.gameObject.CompareTag("placard01") && Input.GetKeyDown(KeyCode.E))
        {
            //tarjetaacceso01.SetActive(true);//activar la tarjeta que esta a dentro;
            preciso.gameObject.SetActive(false);//eliminar el Object Placard por su etiqueta
            //Debug.Log("hiso colliso y presiono p ");
        }
        //no es un prifab solo se duplico el Object y se agrego un TAG diferente a cada uno
        if (preciso.gameObject.CompareTag("placcard02") && Input.GetKeyDown(KeyCode.E)){

            //placard002.SetActive(false);//eliminar llamando a PUBLIC GAMEOBJECT//otra manera larga
            preciso.gameObject.SetActive(false);
        }
        if (preciso.gameObject.CompareTag("placar03") && Input.GetKeyDown(KeyCode.E))
        {
            preciso.gameObject.SetActive(false);
        }
        if(preciso.gameObject.CompareTag("placardcaida1") && Input.GetKeyDown(KeyCode.E))//condicion compuesta
        {//llmar a OBJCET enlazado PISO1 para que se elimine
            piso1.SetActive(false);
        }
        if(preciso.gameObject.CompareTag("placardcaida2") && Input.GetKeyDown(KeyCode.E))
        {//entra y se ejecuta
            piso2.SetActive(false);
           
        }

        if (preciso.gameObject.CompareTag("tarjetaacceso02") && Input.GetKeyDown(KeyCode.E))
        {
            puerta04.SetActive(false);//desactivar puerta04
            preciso.gameObject.SetActive(false);
            //Destroy(preciso.gameObject);elimina de la Ram la tarjeta02
        }

        if (preciso.gameObject.CompareTag("piso11"))
        {
            rapidezDesplazamiento = 3;
            magnitudSalto = 3;
            ////GestorDeAudio.instancia.ReproducirSonido("pisogravedadV05");
            ////GestorDeAudio.instancia.PausarSonido("pisogravedadV02Música1");
        }
        else
        {
            magnitudSalto = 6;
            //rapidezDesplazamiento = 4;
        }

        if(preciso.gameObject.CompareTag("tarjetaacceso03") && Input.GetKeyDown(KeyCode.E))
        {
            //puerta07.SetActive(false);
            rotapuertascript.girarPuerta();//llamar al metodo del scrip para que se ejecute;
            preciso.gameObject.SetActive(false);

        }
        if(preciso.gameObject.CompareTag("guardarpartida")&& Input.GetKeyDown(KeyCode.G)){
            //GestorPersistencia.instancia.data.vidaJugador = vida;
            //GestorPersistencia.instancia.data.chalecoJugador = chaleco;
            // guadarDatos();
            //SetearTextos();//guardar los cambios en el canvas
            // GestorPersistencia.instancia.data.posicionJugador.aVector =gameObject.transform.position;
            // GestorPersistencia.instancia.GuardarDataPersistencia();//guardar la partida
            //guadarDatos();
            //gestordatos.GuardarDataPersistencia();
            //GestorPersistencia.instancia.GuardarDataPersistencia();
            guadarDatos();
            SetearTextos();
        }
        if (preciso.gameObject.CompareTag("tarjetaacceso01") && Input.GetKeyDown(KeyCode.E))
        {
            Destroy(puerta1);
            preciso.gameObject.SetActive(false);
        }
        if (preciso.gameObject.CompareTag("tarjetaacceso04") && Input.GetKeyDown(KeyCode.E))
        {
            puerta04B.SetActive(false);
            preciso.gameObject.SetActive(false);
        }

      
    }
    private void guadarDatos()
    {
        //se guardan los datos que estan presentados si esta activados durante el juego
        //PlayerPrefs.SetFloat("posx", transform.position.x);
        //PlayerPrefs.SetFloat("posy", transform.position.y);
        //PlayerPrefs.SetFloat("posz", transform.position.z);
        PlayerPrefs.SetInt("vida", vida);//la vidaJugador se guarda en la llave "vida"
        PlayerPrefs.SetInt("chaleco10", chaleco);
        PlayerPrefs.SetInt("puntajekill", puntos);

        PlayerPrefs.SetFloat("posicionX", transform.position.x);
        PlayerPrefs.SetFloat("posicionY", transform.position.y);
        PlayerPrefs.SetFloat("posicionZ", transform.position.z);
        Debug.Log("se guardaron los datos");
        SetearTextos();//se guarda en el canvas

    }
    private void cargarDatos()
    {
        //float posx = PlayerPrefs.GetFloat("posx", transform.position.x);
        //float posy = PlayerPrefs.GetFloat("posy", transform.position.y);
        //float posz = PlayerPrefs.GetFloat("posz", transform.position.z);
        vida = PlayerPrefs.GetInt("vida", vida);//lo va a cargar en la misma vidaJugador
        chaleco = PlayerPrefs.GetInt("chaleco10", chaleco);
        puntos = PlayerPrefs.GetInt("puntajekill", puntos);

       
        posX = PlayerPrefs.GetFloat("posicionX");
        posY = PlayerPrefs.GetFloat("posicionY");
        posZ = PlayerPrefs.GetFloat("posicionZ");

        Posicion.x = posX;
        Posicion.y = posY;
        Posicion.z = posZ;
        transform.position = Posicion;
        Debug.Log("Se cargaron los datos");
        SetearTextos();//se van a cargar los cambios en el canvas

    }
    IEnumerator temporizador(float time)//corrutina para la rapidez
    {
        yield return new WaitForSeconds(time);
        rapidezDesplazamiento = 5;//cambio realiza sobre dicha variable.
    }

}
