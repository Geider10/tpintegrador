﻿//camapo de fuerza de enmigo5
using UnityEngine;

public class ProteccionEnemigo5 : MonoBehaviour
{
    public int vidacampo;//public para que sea visible desde otro script
    void Start()
    {
        vidacampo = 200;//igual 200 en ispector
    }
    private void recibirDaño()
    {
        vidacampo -= 20;
        if (vidacampo <= 0)
        {
            Destroy(gameObject);//se va destruir Hijo Object de Padre Enemigo5
        }
    }
    private void recibirDañoEscopeta()
    {
        vidacampo -= 15;
        if (vidacampo <= 0)
        {
            Destroy(gameObject);
        }
        
    }
    private void recibirDañoCuchillo()
    {
        vidacampo -= 10;
        if (vidacampo <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider colision)//detectar collision solo si es Hijo 
    {
        if (colision.gameObject.CompareTag("bala"))
        {
            Debug.Log("Hiso colision con bala");
            recibirDaño();
        }
        if (colision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
            Debug.Log("Hiso colsion con balaescopeta");

        }
        if (colision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
            Debug.Log("Hiso colsion con balacuchillo");

        }

    }

}
