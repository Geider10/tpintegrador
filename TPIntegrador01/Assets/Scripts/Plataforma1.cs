﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma1 : MonoBehaviour
{
    private bool tengoQueBajar;
    private int rapidez = 3;

    void Update()
    {
        if (transform.position.y >= 5)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= -12)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
