﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GestorPersistencia : MonoBehaviour
{
    public static GestorPersistencia instancia;
    public DataPersistencia data;
    public DataPersistencia2 data2;
    string archivoDatos = "save.dat";
    public GameObject jugador;
    public ControlJugador variables;
    private void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

       // CargarDataPersistencia();
      

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            CargarDataPersistencia();
            //variables.SetearTextos();
        }//cambiar  a Jugador;
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    GuardarDataPersistencia();
        //}
    }

    public void GuardarDataPersistencia()
    {
        data.jugadorposicion = new DataPersistencia.Punto(jugador.transform.position);
        //GestorPersistencia.instancia.data2.vidaJugador = variables.vida;
        variables.SetearTextos();
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        //Debug.Log("Datos guardados");
        //GestorPersistencia.instancia.data.vidaJugador = scriptJugador.vida;
        //scriptJugador.SetearTextos();
    }

    public void CargarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();
            //posta//////jugador.transform.position = new Vector3(data.jugadorposicion.x, data.jugadorposicion.y, data.jugadorposicion.z);
            //GestorPersistencia.instancia.data2.vidaJugador = variables.vida;//vida no cambia=200;
            //variables.vida = GestorPersistencia.instancia.data2.vidaJugador;//vida=0
            ///variables.vida = data2.vidaJugador;// vida =0
            //data2.vidaJugador = variables.vida;vida no cambia=200
            //data2.vidaJugador = data2.vidaJugador;se anula
            //data2.vidaJugador = GestorPersistencia.instancia.data2.vidaJugador; se anula
            //GestorPersistencia.instancia.data2.vidaJugador = GestorPersistencia.instancia.data2.vidaJugador; se anula
            //variables.SetearTextos();
            //Debug.Log("Datos cargados broo");
            
        }
       
    }

}
[System.Serializable]
public class DataPersistencia2
{
    public int vidaJugador;//iniciar
    public int chalecoJugador;
    //public Punto posicionJugador;
    //public Punto posicionEnemigo;

    public DataPersistencia2()
    {
        vidaJugador = 0;
        chalecoJugador = 0;

    }
}

[System.Serializable]
public class DataPersistencia
{
    public Punto jugadorposicion;

    [System.Serializable]
    public class Punto
    {
        public float x;
        public float y;
        public float z;

        public Punto(Vector3 p)
        {
            x = p.x;
            y = p.y;
            z = p.z;
        }

        public Vector3 aVector()
        {
            return new Vector3(x, y, z);
        }

    }
}


