﻿
using UnityEngine;

public class Enemigo4 : MonoBehaviour
{
    private int rapidez = 20;
    private bool tengoQueBajar;
    private int vida;
    public ControlJugador puntajevariable;
    void Start()
    {
        vida = 40;
    }

    void Update()
    {
        //siempr se mueve sobre las coordenadas, solo cambia el lugar del nivel
        if (transform.position.x >= -55)
        {
            tengoQueBajar = true;
        }
        if (transform.position.x <= -80)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }
    void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }
    private void recibirDaño()
    {
        vida -= 20;
        if (vida <= 0)
        {//enemigos infinitos
            puntajevariable.puntos += 5;
            puntajevariable.SetearTextos();
            gameObject.SetActive(false);//desactivar para que se puedan activar otra vez
        }
    }
    private void recibirDañoEscopeta()
    {
        vida -= 15;
        if (vida <= 0)
        {//enemigos infinitos
            puntajevariable.puntos += 5;
            puntajevariable.SetearTextos();
            gameObject.SetActive(false);//desactivar para que se puedan activar otra vez
        }
    }
    private void recibirDañoCuchillo()
    {
        vida -= 10;
        if (vida <= 0)
        {//enemigos infinitos
            puntajevariable.puntos += 5;
            puntajevariable.SetearTextos();
            gameObject.SetActive(false);//desactivar para que se puedan activar otra vez
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
        }
    }
}
