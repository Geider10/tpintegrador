﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{

    public void EscenaJuego()//se pueda acceder desde el button
    {
        SceneManager.LoadScene("NivelOficcial01");
    }
    public void SalirJuego()
    {
        Debug.Log("Salida correcta del Juego");
        Application.Quit();//comando para salir desde la build
    }
}
