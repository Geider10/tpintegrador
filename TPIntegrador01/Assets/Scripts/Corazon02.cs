﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corazon02 : MonoBehaviour
{
    public int corazon2;
    void Start()
    {
        corazon2 = 5;
    }
    private void recibirDaño()
    {
        corazon2 -= 20;
        if (corazon2 <= 0)
        {
            Destroy(gameObject);
        }
    }
    private void recibirDañoEscopeta()
    {
        corazon2 -= 15;
        if (corazon2 <= 0)
        {
            Destroy(gameObject);
        }
    }
    private void recibirDañoCuchillo()
    {
        corazon2-= 10;
        if (corazon2 <= 0)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
            Debug.Log("hiso collison con bala");
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
            Debug.Log("hiso collison con balaescopeta");
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
            Debug.Log("hiso collison con balacuchillo");
        }
    }
}
