﻿//enemigo 2 potente patrulla rapido en dicha posicion 
using UnityEngine;

public class Enemigo2 : MonoBehaviour
{
    private int vida;
    private bool tengoQueBajar;
    private int rapidez = 10;
    GameObject puerta02;
    public ControlJugador puntajevariable;
    void Awake()
    {
        puerta02 = GameObject.Find("Puerta02");//nombre de mi Object dentro de la jerarquia
    }
    void Start()
    {
        vida = 250;
        
    }

    void Update()
    {
        if (transform.position.z >= 34)//cuando al alcance la posicion mayor baja
        {
            tengoQueBajar = true;
        }
        if (transform.position.z <= 0)//se ejecuta y viceverza
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
        
    }
    void Subir()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }
    private void recibirDaño()
    {
        vida -= 20;
        if (vida <= 0)
        {
            puntajevariable.puntos += 20;
            puntajevariable.SetearTextos();
            Debug.Log("se actualizo los puntos");
            Destroy(gameObject);//destruir Object del Scrip Enemigo2
            puerta02.SetActive(false);//desactivar Object Puerta"es prifab"
        }
    }
    private void recibirDañoEscopeta()
    {
        vida -= 15;
        if (vida <= 0)
        {
            puntajevariable.puntos += 20;
            puntajevariable.SetearTextos();
            Debug.Log("se actualizo los puntos");
            Destroy(gameObject);//destruir Object del Scrip Enemigo2
            puerta02.SetActive(false);//desactivar Object Puerta"es prifab"
        }
    }
    private void recibirDañoCuchillo()
    {
        vida -= 10;
        if (vida <= 0)
        {
            puntajevariable.puntos += 20;
            puntajevariable.SetearTextos();
            Debug.Log("se actualizo los puntos");
            Destroy(gameObject);//destruir Object del Scrip Enemigo2
            puerta02.SetActive(false);//desactivar Object Puerta"es prifab"
        }
    }

    private void OnCollisionEnter(Collision collision)
    {//el Enemigo1 va a detectar si hiso collisom con el Object Proyectil
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
        }
    }
}
