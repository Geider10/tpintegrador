﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        GestorDeAudio.instancia.PausarSonido("musicaGame");
    }
    public void resgresarMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
