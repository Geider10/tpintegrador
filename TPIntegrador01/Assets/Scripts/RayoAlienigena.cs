﻿
using UnityEngine;

public class RayoAlienigena : MonoBehaviour
{
    private int rapidez = 30;
    private bool tengoQueBajar ;

    void Update()
    {
        if (transform.position.x >= 26)
        {
            tengoQueBajar = true;
        }
        if (transform.position.x <= -76)//Enemigo empieza de cualquier posicion
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }
}
