﻿
using UnityEngine;

public class Enemigo3Max : MonoBehaviour
{
    private int vida;
    private bool tengoQueBajar;
    private int rapidez = 6;
    GameObject puerta03;
    GameObject tarjetaAcceso02;
    public ControlJugador puntajevariable;

    void Awake()
    {
        puerta03 = GameObject.Find("Puerta03");
        tarjetaAcceso02 = GameObject.Find("TarjetaAcceso02");
    }
    void Start()
    {
        vida = 100;
        tarjetaAcceso02.SetActive(false);//iniciar tarjeta02 oculta
    }
    void Update()
    {
        if (transform.position.y >= 10)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= -3)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }
    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
    private void recibirDaño()
    {
        vida -= 20;
        if (vida <= 0)
        {
            puntajevariable.puntos += 15;
            puntajevariable.SetearTextos();
            Destroy(gameObject);
            puerta03.SetActive(false);//abrir la puerta 03;
            tarjetaAcceso02.SetActive(true);//activar tarjeta02
        }
    }
    private void recibirDañoEscopeta()
    {
        vida -= 15;
        if (vida <= 0)
        {
            puntajevariable.puntos += 15;
            puntajevariable.SetearTextos();
            Destroy(gameObject);
            puerta03.SetActive(false);//abrir la puerta 03;
            tarjetaAcceso02.SetActive(true);//activar tarjeta02
        }
    }
    private void recibirDañoCuchillo()
    {
        vida -= 10;
        if (vida <= 0)
        {
            puntajevariable.puntos += 15;
            puntajevariable.SetearTextos();
            Destroy(gameObject);
            puerta03.SetActive(false);//abrir la puerta 03;
            tarjetaAcceso02.SetActive(true);//activar tarjeta02
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
        }
    }
}
