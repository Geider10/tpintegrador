﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corazon01 : MonoBehaviour
{
    public int corazon1;//cuando inicie la partida va a leer CORAZON1 5;
    void Start()
    {
        corazon1 = 5;
    }
    private void recibirDaño()
    {
        corazon1 -= 20;
        if (corazon1 <= 0)
        {
            Destroy(gameObject);
        }
    }
    private void recibirDañoEscopeta()
    {
        corazon1 -= 15;
        if (corazon1 <= 0)
        {
            Destroy(gameObject);
        }
    }
    private void recibirDañoCuchillo()
    {
        corazon1 -= 10;
        if (corazon1 <= 0)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
            Debug.Log("Hiso colision con bala");
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
            Debug.Log("Hiso colsion con balaescopeta");
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
            Debug.Log("Hiso colsion con balacuchillo");
        }
    }
}
