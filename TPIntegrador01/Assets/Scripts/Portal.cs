﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Transform portalSalida;
    public GameObject jugador;
    private void OnTriggerEnter(Collider collision)
    {//Jugador hace collision con portalEntrada te llava a la position de portalSalida.
        jugador.transform.position = portalSalida.transform.position;
    }
}
