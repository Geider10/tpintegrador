﻿//no tiene vida muere cuando matan a todos sus corazones;
using UnityEngine;

public class Enemigo6 : MonoBehaviour
{
    private int rapidez = 4;
    GameObject jugador;
    GameObject puerta10;
    public ControlJugador variablepuntaje;
    public Corazon01 variablescorazon1;
    public Corazon02 variablescorazon2;
    void Awake()
    {
        jugador = GameObject.Find("Jugador");
        puerta10 = GameObject.Find("Puerta10");
    }

    void Update()
    {
        transform.LookAt(jugador.transform);//se mueve segun la posicion del Jugador
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        transform.Translate(rapidez * Vector3.right * Time.deltaTime);
    }
    private void recibirDaño()
    {
        Destroy(gameObject);
        puerta10.SetActive(false);
        variablepuntaje.puntos += 30;
        variablepuntaje.SetearTextos();//actualizar los cambios del TXT PUNTAJE

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            if(variablescorazon1.corazon1<=0 && variablescorazon2.corazon2 <= 0)
            {
                recibirDaño();
            }
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            if (variablescorazon1.corazon1 <= 0 && variablescorazon2.corazon2 <= 0)
            {
                recibirDaño();
            }
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            if (variablescorazon1.corazon1 <= 0 && variablescorazon2.corazon2 <= 0)
            {
                recibirDaño();
            }
            
        }
    }
}
