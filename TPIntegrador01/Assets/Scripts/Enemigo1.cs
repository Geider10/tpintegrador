﻿//Enemigo 1 "bomba"seguir al Jugador y si choca le explota(baja mucha vida)
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    private int vida;
    private int rapidez = 10;
    GameObject jugador;
    public ControlJugador puntajevariable;
    void Start()
    {
        vida = 100;
        jugador = GameObject.Find("Jugador");//llamar

    }
    void Update()
    {
        transform.LookAt(jugador.transform);//se mueve segun la posicion del Jugador
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void recibirDaño()//metodo para llamar en la collision//arma pistola
    {
        vida = vida - 20;//los tirros le descuentan de a 20
        if (vida <= 0)
        {
            puntajevariable.puntos += 10;
            puntajevariable.SetearTextos();
            desaparecer();
        }
    }
    private void recibirDañoEscopeta()
    {
        vida -=15;//los tirros le descuentan de a 15
        if (vida <= 0)
        {
            puntajevariable.puntos += 10;
            puntajevariable.SetearTextos();
            desaparecer();
        }
    }
    private void recibirDañoCuchillo()
    {
        vida -=10;//cada cuchillo saca 10
        if (vida <= 0)
        {
            puntajevariable.puntos += 10;
            puntajevariable.SetearTextos();
            desaparecer();
        }
    }


    private void OnCollisionEnter(Collision collision)
    {//el Enemigo1 va a detectar si hiso collisom con etiquetas
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
        if (collision.gameObject.CompareTag("jugador")==true)//busca la collision con Object Jugador
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
        }
    }


private void desaparecer()//metodo en comun
    {
        Destroy(gameObject);
    }
}
