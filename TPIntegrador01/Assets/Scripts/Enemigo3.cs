﻿
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{
    private int vida;
    private bool tengoQueBajar;
    private int rapidez = 5;
    GameObject enemigo4;
    GameObject enemigo41;
    GameObject enemigo42;
    GameObject enemigo43;
    public ControlJugador puntajevariable;


    void Awake()
    {
        enemigo4 = GameObject.Find("Enemigo4");
        enemigo41 = GameObject.Find("Enemigo4.1");
        enemigo42= GameObject.Find("Enemigo4.2");
        enemigo43= GameObject.Find("Enemigo4.3");
    }

    void Start()
    {
        vida = 80;
        enemigo4.SetActive(false);
        enemigo41.SetActive(false);
        enemigo42.SetActive(false);
        enemigo43.SetActive(false);
    }
    void Update()
    {
        if (transform.position.y >= 10)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= -3)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }

    private void recibirDaño()
    {
        vida -= 20;
        if (vida <= 0)
        {
            puntajevariable.puntos += 15;
            puntajevariable.SetearTextos();
            Destroy(gameObject);
        }
    }
    private void recibirDañoEscopeta()
    {
        vida -= 15;
        if (vida <= 0)
        {
            puntajevariable.puntos += 15;
            puntajevariable.SetearTextos();
            Destroy(gameObject);
        }
    }
    private void recibirDañoCuchillo()
    {
        vida -= 10;
        if (vida <= 0)
        {
            puntajevariable.puntos += 15;
            puntajevariable.SetearTextos();
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))//enemigo3 recibe collision de bala
        {
            recibirDaño();
            enemigo4.SetActive(true);//despiertan todos los bichos(no hay bug)
            enemigo41.SetActive(true);
            enemigo42.SetActive(true);
            enemigo43.SetActive(true);
        }
        //Enemigo3 recibio collsion de Object con TAG
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            recibirDañoEscopeta();
            enemigo4.SetActive(true);//despiertan todos los bichos(no hay bug)
            enemigo41.SetActive(true);
            enemigo42.SetActive(true);
            enemigo43.SetActive(true);
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            recibirDañoCuchillo();
            enemigo4.SetActive(true);//despiertan todos los bichos(no hay bug)
            enemigo41.SetActive(true);
            enemigo42.SetActive(true);
            enemigo43.SetActive(true);
        }
    }
}
