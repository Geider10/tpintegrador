﻿//Enemigo final 
using UnityEngine;

public class Enemigo5 : MonoBehaviour
{
    private int rapidez = 6;
    private int vida;
    GameObject jugador;
    public  GameObject tarjetaacceso03;
    public ProteccionEnemigo5 variables;//acceder a las varabiables publicas de ese script
    public ControlJugador puntajevariable;
    void Start()
    {
        vida = 600;//muere con 30 disparos de a 20
        jugador = GameObject.Find("Jugador");
        //tarjetaacceso03 = GameObject.Find("TarjetaAcceso03");
        tarjetaacceso03.SetActive(false);
    }
    void Update()
    {
        transform.LookAt(jugador.transform);//se mueve segun la posicion del Jugador
        transform.Translate(rapidez * Vector3.up * Time.deltaTime);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
    private void recibirDaño()
    {
        vida -= 20;
        if (vida <= 0)
        {
            puntajevariable.puntos += 30;
            puntajevariable.SetearTextos();
            //Destroy(gameObject);//hace referencia a Enemigo5
            gameObject.SetActive(false);//tambien se desactiiva hijo tarjeta(bug)
            tarjetaacceso03.SetActive(true);
        }
    }
    private void recibirDañoEscopeta()
    {
        vida -= 15;//recibir daño de 15 por cada tirro de escopeta
        if (vida <= 0)
        {
            puntajevariable.puntos += 30;
            puntajevariable.SetearTextos();
            //Destroy(gameObject);//hace referencia a Enemigo5
            gameObject.SetActive(false);//tambien se desactiiva hijo tarjeta(bug)
            tarjetaacceso03.SetActive(true);
        }
    }
    private void recibirDañoCuchillo()
    {
        vida -= 10;
        if (vida <= 0)
        {
            puntajevariable.puntos += 30;
            puntajevariable.SetearTextos();
            //Destroy(gameObject);//hace referencia a Enemigo5
            gameObject.SetActive(false);//tambien se desactiiva hijo tarjeta(bug)
            tarjetaacceso03.SetActive(true);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {    
            if(variables.vidacampo <=0)//si su esfera se queda sin vida, ricien se ejecuta para recibirDaño
            {
                recibirDaño();
            }
        }
        if (collision.gameObject.CompareTag("balaescopeta"))
        {
            if (variables.vidacampo <= 0)
            {
                recibirDañoEscopeta();
            }
        }
        if (collision.gameObject.CompareTag("balacuchillo"))
        {
            if (variables.vidacampo <= 0)
            {
                recibirDañoCuchillo();
            }
        }
    }
}
