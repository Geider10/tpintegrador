﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




extern Il2CppGenericClass* const s_Il2CppGenericTypes[];
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[];
extern const Il2CppGenericMethodFunctionsDefinitions s_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
IL2CPP_EXTERN_C_CONST int32_t* g_FieldOffsetTable[];
IL2CPP_EXTERN_C_CONST Il2CppTypeDefinitionSizes* g_Il2CppTypeDefinitionSizesTable[];
extern void** const g_MetadataUsages[];
extern const Il2CppMetadataRegistration g_MetadataRegistration;
const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	14481,
	s_Il2CppGenericTypes,
	2544,
	g_Il2CppGenericInstTable,
	23907,
	s_Il2CppGenericMethodFunctions,
	26448,
	g_Il2CppTypeTable,
	25234,
	g_Il2CppMethodSpecTable,
	3616,
	g_FieldOffsetTable,
	3616,
	g_Il2CppTypeDefinitionSizesTable,
	15380,
	g_MetadataUsages,
};
