﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ControlCamara::Start()
extern void ControlCamara_Start_m3E41AB41B48C305B801CE88E1088E30F6B48065C (void);
// 0x00000002 System.Void ControlCamara::Update()
extern void ControlCamara_Update_m739B8939B4C7E54D46E16102A9EBEE6DF983FFFD (void);
// 0x00000003 System.Void ControlCamara::.ctor()
extern void ControlCamara__ctor_m954699EF3390352A462FE175B54EEB6A85882E34 (void);
// 0x00000004 System.Void ControlJugador::Awake()
extern void ControlJugador_Awake_m177F022BE32DC1EFD64F026C1BB4A9782A40973C (void);
// 0x00000005 System.Void ControlJugador::Start()
extern void ControlJugador_Start_m4E2BC1B8AEF94CFA13FA0A675ECC6CDD7C9E2546 (void);
// 0x00000006 System.Void ControlJugador::SetearTextos()
extern void ControlJugador_SetearTextos_m5E66A99531954232ACE79818FD29A2FC8B8580EF (void);
// 0x00000007 System.Void ControlJugador::Update()
extern void ControlJugador_Update_m3C5ABBBBF954C1FB58E04A8E18F75DF313BE6FE8 (void);
// 0x00000008 System.Void ControlJugador::Pistola()
extern void ControlJugador_Pistola_mBEA2B0A854D01D23DE45AE64005A6F9058885896 (void);
// 0x00000009 System.Void ControlJugador::Escopeta()
extern void ControlJugador_Escopeta_m8C0C0E0DF22DE82B213B0C23057789D6319C5362 (void);
// 0x0000000A System.Void ControlJugador::ArmaCuchillo()
extern void ControlJugador_ArmaCuchillo_m70B5612A4481D7C92AA29A48D2AF572C82961751 (void);
// 0x0000000B System.Void ControlJugador::movimientoPersonaje()
extern void ControlJugador_movimientoPersonaje_m2E9A20461C8AD1AD17973278E19CA9F679A92D92 (void);
// 0x0000000C System.Boolean ControlJugador::EstaEnPiso()
extern void ControlJugador_EstaEnPiso_m1FB74EA8781653CEF46B339AE1FF081E6FB370AB (void);
// 0x0000000D System.Void ControlJugador::OnTriggerStay(UnityEngine.Collider)
extern void ControlJugador_OnTriggerStay_mF42405D867CB49964E16A390ABB33B1972190E49 (void);
// 0x0000000E System.Void ControlJugador::OnCollisionEnter(UnityEngine.Collision)
extern void ControlJugador_OnCollisionEnter_m22C5CB05D3986EE56F0331609C2567921C385320 (void);
// 0x0000000F System.Void ControlJugador::OnCollisionStay(UnityEngine.Collision)
extern void ControlJugador_OnCollisionStay_m09522BF80CEF5F04DC7254EC411EBA0C86C14A1B (void);
// 0x00000010 System.Void ControlJugador::guadarDatos()
extern void ControlJugador_guadarDatos_m3B484AEC6CAE13C0C5896872BCF34EF9E2922D9D (void);
// 0x00000011 System.Void ControlJugador::cargarDatos()
extern void ControlJugador_cargarDatos_mE0A6A798CDBE495550CC687385449FE7547AFDE4 (void);
// 0x00000012 System.Collections.IEnumerator ControlJugador::temporizador(System.Single)
extern void ControlJugador_temporizador_m08B3483C82319E7AD928277333BA56640CCE7882 (void);
// 0x00000013 System.Void ControlJugador::.ctor()
extern void ControlJugador__ctor_m8081D7912A48835FE1D245E0CA7E6DD5D2E868CB (void);
// 0x00000014 System.Void Corazon01::Start()
extern void Corazon01_Start_m89C4976811E85BCB88137DD2B44909CA60048615 (void);
// 0x00000015 System.Void Corazon01::recibirDaUF1o()
extern void Corazon01_recibirDaUF1o_mF6C005DBCB5A3E5ADBBEE6654D9E5485BF00FE51 (void);
// 0x00000016 System.Void Corazon01::recibirDaUF1oEscopeta()
extern void Corazon01_recibirDaUF1oEscopeta_m97F9C9AA604BECE174CA17DE028F1405EB23E032 (void);
// 0x00000017 System.Void Corazon01::recibirDaUF1oCuchillo()
extern void Corazon01_recibirDaUF1oCuchillo_m3AC0622D7EC219F9CA5CE8DBAA60DBDA8C8AE068 (void);
// 0x00000018 System.Void Corazon01::OnTriggerEnter(UnityEngine.Collider)
extern void Corazon01_OnTriggerEnter_mA637476AE5D3826732C8FADB0E4A82280A09B142 (void);
// 0x00000019 System.Void Corazon01::.ctor()
extern void Corazon01__ctor_mA6C8BFA6907304BC4FC3234A17B4F21AF9E363A7 (void);
// 0x0000001A System.Void Corazon02::Start()
extern void Corazon02_Start_m80034F1D8338D15BFDEE519B7C3D3BE39E787455 (void);
// 0x0000001B System.Void Corazon02::recibirDaUF1o()
extern void Corazon02_recibirDaUF1o_mE8FA32AF7F49358E7614B492EB6DA2E597B1D102 (void);
// 0x0000001C System.Void Corazon02::recibirDaUF1oEscopeta()
extern void Corazon02_recibirDaUF1oEscopeta_m04A30A6701B9B857B448C3CBBD6532E574D9F113 (void);
// 0x0000001D System.Void Corazon02::recibirDaUF1oCuchillo()
extern void Corazon02_recibirDaUF1oCuchillo_mE03C843CEEEB530A2198019BB4BDC3D6FF500721 (void);
// 0x0000001E System.Void Corazon02::OnTriggerEnter(UnityEngine.Collider)
extern void Corazon02_OnTriggerEnter_mAD72DCF9C85560B7CB17E23A77B8E9C9F55871D4 (void);
// 0x0000001F System.Void Corazon02::.ctor()
extern void Corazon02__ctor_m67AE92AB35B1A94331206ADAFC9D566E58623E95 (void);
// 0x00000020 System.Void Enemigo1::Start()
extern void Enemigo1_Start_mB31CAB00B80FC7C6CD74372A5C323FC0D67F9E6B (void);
// 0x00000021 System.Void Enemigo1::Update()
extern void Enemigo1_Update_m33D0C5EF344DD7961D6A83CF83668F37BAD23853 (void);
// 0x00000022 System.Void Enemigo1::recibirDaUF1o()
extern void Enemigo1_recibirDaUF1o_mCD1816865144CE7D8183A5BAF85ECA0477B0B195 (void);
// 0x00000023 System.Void Enemigo1::recibirDaUF1oEscopeta()
extern void Enemigo1_recibirDaUF1oEscopeta_m4D2C8884E0DA417B1E8369A977705E8B30F78DF2 (void);
// 0x00000024 System.Void Enemigo1::recibirDaUF1oCuchillo()
extern void Enemigo1_recibirDaUF1oCuchillo_m2BEA2A5560F2DAE696DD489399B1162F9716A4D2 (void);
// 0x00000025 System.Void Enemigo1::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo1_OnCollisionEnter_m03FFA061A7BE4CF07A7C8DEBE5111FBA1424C7F2 (void);
// 0x00000026 System.Void Enemigo1::desaparecer()
extern void Enemigo1_desaparecer_m7E789D5ABEE57043EE924658F3EA63412EF155D9 (void);
// 0x00000027 System.Void Enemigo1::.ctor()
extern void Enemigo1__ctor_mA21518193FCA9FFB7EF696DEE06FB2E43D764BC0 (void);
// 0x00000028 System.Void Enemigo2::Awake()
extern void Enemigo2_Awake_m6C46D97D7FB2BC697DA4FE42824324E6CF061ACA (void);
// 0x00000029 System.Void Enemigo2::Start()
extern void Enemigo2_Start_m35BF6113BB1B50AC721AE2D6994B6D1966229687 (void);
// 0x0000002A System.Void Enemigo2::Update()
extern void Enemigo2_Update_m44F70814888E5DE05A90B7FB04546AACCAE6A5AB (void);
// 0x0000002B System.Void Enemigo2::Subir()
extern void Enemigo2_Subir_m30E108D7898FBBC24EF1A4AFB33D2F3E5AF1B538 (void);
// 0x0000002C System.Void Enemigo2::Bajar()
extern void Enemigo2_Bajar_mAFC69155485E00B1DD71DBD9E9E03B6309C66351 (void);
// 0x0000002D System.Void Enemigo2::recibirDaUF1o()
extern void Enemigo2_recibirDaUF1o_m7A7B24E5557AA906BACC605298FDA52251822541 (void);
// 0x0000002E System.Void Enemigo2::recibirDaUF1oEscopeta()
extern void Enemigo2_recibirDaUF1oEscopeta_m65142986E766A9F54BAA3AB1A0BD32D872D7265E (void);
// 0x0000002F System.Void Enemigo2::recibirDaUF1oCuchillo()
extern void Enemigo2_recibirDaUF1oCuchillo_m4D0E1CB1BAC0999EFBBB5904F893DFE9EC50AEBC (void);
// 0x00000030 System.Void Enemigo2::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo2_OnCollisionEnter_m749039A88D721D9D967AAC772FEFA9A96E5D008D (void);
// 0x00000031 System.Void Enemigo2::.ctor()
extern void Enemigo2__ctor_mC7860BFCA620127B4AD1E68CEDB4DED4A19F4C00 (void);
// 0x00000032 System.Void Enemigo3::Awake()
extern void Enemigo3_Awake_m7C7D8E04AD17EAC0B0279DC643958D2DF517E37F (void);
// 0x00000033 System.Void Enemigo3::Start()
extern void Enemigo3_Start_m813435C85BA38D5505D889870689516DDA40135B (void);
// 0x00000034 System.Void Enemigo3::Update()
extern void Enemigo3_Update_m33C789BECD12FD0BF863525A3EB8EB976AEF3389 (void);
// 0x00000035 System.Void Enemigo3::Subir()
extern void Enemigo3_Subir_mE6E761BCB428B20D3FEFFC0127AB8431D7DDF3E7 (void);
// 0x00000036 System.Void Enemigo3::Bajar()
extern void Enemigo3_Bajar_mF0A49A9D137E8904119082AC368BF24E7820492B (void);
// 0x00000037 System.Void Enemigo3::recibirDaUF1o()
extern void Enemigo3_recibirDaUF1o_mD9AA3DB7A86349D494455CBA42FA4C243B1C363D (void);
// 0x00000038 System.Void Enemigo3::recibirDaUF1oEscopeta()
extern void Enemigo3_recibirDaUF1oEscopeta_mCE00839D3CF8D64CA51E969D3D1F8BE3190F4014 (void);
// 0x00000039 System.Void Enemigo3::recibirDaUF1oCuchillo()
extern void Enemigo3_recibirDaUF1oCuchillo_m626827ADB47A8ABADEF48837DA91A2E628B2B528 (void);
// 0x0000003A System.Void Enemigo3::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo3_OnCollisionEnter_mB8CA7CD60E8A54EA19EB7E591F70FA137B8C0A2C (void);
// 0x0000003B System.Void Enemigo3::.ctor()
extern void Enemigo3__ctor_mDD8DB8EEAD78CE8E3D08C551655FD92CEF29A176 (void);
// 0x0000003C System.Void Enemigo3Max::Awake()
extern void Enemigo3Max_Awake_m5458C526BFC6E1D76DC5D28705144A6AB838F6E8 (void);
// 0x0000003D System.Void Enemigo3Max::Start()
extern void Enemigo3Max_Start_m56EAB6E6F829D143EB3841430606454EB6B0AC1A (void);
// 0x0000003E System.Void Enemigo3Max::Update()
extern void Enemigo3Max_Update_m4E1D7FF20154968B8E4416EA732582011E73138E (void);
// 0x0000003F System.Void Enemigo3Max::Subir()
extern void Enemigo3Max_Subir_m6CC92C48078685BFA4D57DFEDB571C8B569E5C74 (void);
// 0x00000040 System.Void Enemigo3Max::Bajar()
extern void Enemigo3Max_Bajar_mBA583AA518836066DA126E0BA143A7D542FDD407 (void);
// 0x00000041 System.Void Enemigo3Max::recibirDaUF1o()
extern void Enemigo3Max_recibirDaUF1o_m9C66E08BDA1E02DC57D3B37DA1D603EA3C8FE891 (void);
// 0x00000042 System.Void Enemigo3Max::recibirDaUF1oEscopeta()
extern void Enemigo3Max_recibirDaUF1oEscopeta_mB7D5E97007BADDA99781F8C16992A19FA989480C (void);
// 0x00000043 System.Void Enemigo3Max::recibirDaUF1oCuchillo()
extern void Enemigo3Max_recibirDaUF1oCuchillo_mBA390A4532206A889D81886C39A03030703878FD (void);
// 0x00000044 System.Void Enemigo3Max::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo3Max_OnCollisionEnter_m5DFA7A9EB884786348F4A193F5CFFB550B84B17E (void);
// 0x00000045 System.Void Enemigo3Max::.ctor()
extern void Enemigo3Max__ctor_m05D9F06810B5441ADC4AA06DC71A8F548CB2223A (void);
// 0x00000046 System.Void Enemigo4::Start()
extern void Enemigo4_Start_m27F81CFCA504E9022AE6AF8876D9A004E17D5F59 (void);
// 0x00000047 System.Void Enemigo4::Update()
extern void Enemigo4_Update_m3708BAD12208B561D4AC601747D89D524366E96F (void);
// 0x00000048 System.Void Enemigo4::Subir()
extern void Enemigo4_Subir_m57164D61C86EADC333CB6D4803FD59322DAD0083 (void);
// 0x00000049 System.Void Enemigo4::Bajar()
extern void Enemigo4_Bajar_m72317D00F0F5CFE3692CE64F21659FE0B1EA89CA (void);
// 0x0000004A System.Void Enemigo4::recibirDaUF1o()
extern void Enemigo4_recibirDaUF1o_mE4B65374E232096AB3D3BF0E91232D71E3149AC9 (void);
// 0x0000004B System.Void Enemigo4::recibirDaUF1oEscopeta()
extern void Enemigo4_recibirDaUF1oEscopeta_m0D8EB29C9235471BB699E344551C6FFFD8E71228 (void);
// 0x0000004C System.Void Enemigo4::recibirDaUF1oCuchillo()
extern void Enemigo4_recibirDaUF1oCuchillo_m5E4C55A63E39770906EFA41CE9390A56058B0D1B (void);
// 0x0000004D System.Void Enemigo4::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo4_OnCollisionEnter_m61885496DFFD3C98849D6D18192CD54827D3655A (void);
// 0x0000004E System.Void Enemigo4::.ctor()
extern void Enemigo4__ctor_mA3F76054934D3C2BF41FBC125F16EF4AF4CEDA2C (void);
// 0x0000004F System.Void Enemigo5::Start()
extern void Enemigo5_Start_m86ADB0305AA1834EBF228D5AFF07549496C43236 (void);
// 0x00000050 System.Void Enemigo5::Update()
extern void Enemigo5_Update_m7C523FD21C8373C521A20CB9AEE6AFD7E710F7EF (void);
// 0x00000051 System.Void Enemigo5::recibirDaUF1o()
extern void Enemigo5_recibirDaUF1o_m69B1CFEF055EBB96F33F744B42D4876B0D1A748E (void);
// 0x00000052 System.Void Enemigo5::recibirDaUF1oEscopeta()
extern void Enemigo5_recibirDaUF1oEscopeta_mA0BBFF6347164C2CE7EF8156C1D8B44A8E0383BB (void);
// 0x00000053 System.Void Enemigo5::recibirDaUF1oCuchillo()
extern void Enemigo5_recibirDaUF1oCuchillo_m69F1EA023C549BBAA8328F9381F3F53AC975E606 (void);
// 0x00000054 System.Void Enemigo5::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo5_OnCollisionEnter_m60E54D6A501AA9917B63F3F2D329BBC1B4676F54 (void);
// 0x00000055 System.Void Enemigo5::.ctor()
extern void Enemigo5__ctor_m0D27366C75DBC564093C8135A572D861FDAA9604 (void);
// 0x00000056 System.Void Enemigo6::Awake()
extern void Enemigo6_Awake_m36AC7F1CD71CEE665745CAC6475FD4A89512824C (void);
// 0x00000057 System.Void Enemigo6::Update()
extern void Enemigo6_Update_m3B43D18DA665CFDEFCAE0EF1F0C68AB0843BC163 (void);
// 0x00000058 System.Void Enemigo6::recibirDaUF1o()
extern void Enemigo6_recibirDaUF1o_m98CDE7098974B4C0D1D6DBB12DEF793313181394 (void);
// 0x00000059 System.Void Enemigo6::OnCollisionEnter(UnityEngine.Collision)
extern void Enemigo6_OnCollisionEnter_m31D20DA9FA4A058547CB4D9FA6A3E78F05E7A442 (void);
// 0x0000005A System.Void Enemigo6::.ctor()
extern void Enemigo6__ctor_mE0458C87A3097E91513714789025333FF17993C2 (void);
// 0x0000005B System.Void GameOver::Start()
extern void GameOver_Start_mBEEA4334176BA871E2CD30E445D80AF8C0044AB1 (void);
// 0x0000005C System.Void GameOver::resgresarMainMenu()
extern void GameOver_resgresarMainMenu_m7371FDE4F04392697FCA7A89BA663CDE29CD5453 (void);
// 0x0000005D System.Void GameOver::.ctor()
extern void GameOver__ctor_m4E80B334B510F1E21C8C80FE0DAE28A0058B5E3D (void);
// 0x0000005E System.Void GestorDeAudio::Awake()
extern void GestorDeAudio_Awake_m84CFA483ACF5BF5311A6ECE229C634A87EA65724 (void);
// 0x0000005F System.Void GestorDeAudio::ReproducirSonido(System.String)
extern void GestorDeAudio_ReproducirSonido_mAD4D45E1475BE5CD50434BA9A50F290DC25441C0 (void);
// 0x00000060 System.Void GestorDeAudio::PausarSonido(System.String)
extern void GestorDeAudio_PausarSonido_m1380B3D1103DB17218DB7288B04E857F3A329B1C (void);
// 0x00000061 System.Void GestorDeAudio::.ctor()
extern void GestorDeAudio__ctor_mC1EBE323FE9A42E91A85736CA022B1D2161D64AC (void);
// 0x00000062 System.Void GestorPersistencia::Awake()
extern void GestorPersistencia_Awake_m018F88D5F06839EEF22D68C13C6EEE78F287B72C (void);
// 0x00000063 System.Void GestorPersistencia::Update()
extern void GestorPersistencia_Update_m7FB4A1B54B1C87DC46947B5CED41754D3375D334 (void);
// 0x00000064 System.Void GestorPersistencia::GuardarDataPersistencia()
extern void GestorPersistencia_GuardarDataPersistencia_m83BFFB9EAB6848F0A1918F79F07D2808857DCCE4 (void);
// 0x00000065 System.Void GestorPersistencia::CargarDataPersistencia()
extern void GestorPersistencia_CargarDataPersistencia_m8DE4A76A21E105ADDFAF8B1021EC18044A7CA991 (void);
// 0x00000066 System.Void GestorPersistencia::.ctor()
extern void GestorPersistencia__ctor_mB9BE796A49F0A1104A8A12E734002E157C8C08A7 (void);
// 0x00000067 System.Void DataPersistencia2::.ctor()
extern void DataPersistencia2__ctor_m93694CD4C749A07EB198AF448C84DBC0DED01E57 (void);
// 0x00000068 System.Void DataPersistencia::.ctor()
extern void DataPersistencia__ctor_m674C6EBD140096D14760924D4479E74CB7BB08F5 (void);
// 0x00000069 System.Void MainMenu::EscenaJuego()
extern void MainMenu_EscenaJuego_m50BA49601483B40DD16EBB8C1C54EDE7953D5746 (void);
// 0x0000006A System.Void MainMenu::SalirJuego()
extern void MainMenu_SalirJuego_m0C079DA9B577D1846D000D87C00A60593B83AD40 (void);
// 0x0000006B System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m63F945D965550BD614DCD2AE7F7489D4F28C5B30 (void);
// 0x0000006C System.Void MusicaFondo::Start()
extern void MusicaFondo_Start_m8390F376955B1FEBFDFEBD9167BEA33E9EBFA002 (void);
// 0x0000006D System.Void MusicaFondo::.ctor()
extern void MusicaFondo__ctor_m631372D66DACD2ED55A735DA535845725DC98007 (void);
// 0x0000006E System.Void Plataforma1::Update()
extern void Plataforma1_Update_mDBD0B17D103982652E6F0A891CDA6570A906308D (void);
// 0x0000006F System.Void Plataforma1::Subir()
extern void Plataforma1_Subir_m9CF73D2EBFC1835259E53B0F93F033C4D65069A7 (void);
// 0x00000070 System.Void Plataforma1::Bajar()
extern void Plataforma1_Bajar_m37270E0093D16C952E4CDDDA027533761BD71693 (void);
// 0x00000071 System.Void Plataforma1::.ctor()
extern void Plataforma1__ctor_mF92A649D1FCFBC0939A28B8CE7637E394DDEA243 (void);
// 0x00000072 System.Void Portal::OnTriggerEnter(UnityEngine.Collider)
extern void Portal_OnTriggerEnter_mAD6086195F0290D3EB350FFCB0EABF6233B338D2 (void);
// 0x00000073 System.Void Portal::.ctor()
extern void Portal__ctor_m6117B0C40EA244EFE65192DFF469E7C461EAC42F (void);
// 0x00000074 System.Void ProteccionEnemigo5::Start()
extern void ProteccionEnemigo5_Start_m1137A9C6FE1184EC9E6C313386151BDF72DFED0B (void);
// 0x00000075 System.Void ProteccionEnemigo5::recibirDaUF1o()
extern void ProteccionEnemigo5_recibirDaUF1o_m937768DAE0E4D4C930097F60B6B49A680546A7D3 (void);
// 0x00000076 System.Void ProteccionEnemigo5::recibirDaUF1oEscopeta()
extern void ProteccionEnemigo5_recibirDaUF1oEscopeta_m832E6D068EECC4D858752284F36B850A9F1FB63D (void);
// 0x00000077 System.Void ProteccionEnemigo5::recibirDaUF1oCuchillo()
extern void ProteccionEnemigo5_recibirDaUF1oCuchillo_mB474E273FF636395A2DACDF52DC0C851B4B691CA (void);
// 0x00000078 System.Void ProteccionEnemigo5::OnTriggerEnter(UnityEngine.Collider)
extern void ProteccionEnemigo5_OnTriggerEnter_mE96FD5382EE73DC0A88E33F33537FAAE6579CE21 (void);
// 0x00000079 System.Void ProteccionEnemigo5::.ctor()
extern void ProteccionEnemigo5__ctor_m1DE2D8822072FB3B4734FD95346F68B240CD77B3 (void);
// 0x0000007A System.Void RayoAlienigena::Update()
extern void RayoAlienigena_Update_m48B6347E5A0E2F5FBBD33DEB7017DE291979E6C7 (void);
// 0x0000007B System.Void RayoAlienigena::Subir()
extern void RayoAlienigena_Subir_m7CD54F979AA230426467D7CC6E4C0FA6C97E0025 (void);
// 0x0000007C System.Void RayoAlienigena::Bajar()
extern void RayoAlienigena_Bajar_m0FDD3918592B811B449B763B28D0611DB8116DD3 (void);
// 0x0000007D System.Void RayoAlienigena::.ctor()
extern void RayoAlienigena__ctor_m90D44D06F25A273B0BCD5D87E04CD70F4B791F20 (void);
// 0x0000007E System.Void RotarPisos::Update()
extern void RotarPisos_Update_m36A19CB92B0CC9DF80D2D3D81D878CCFF7927FFF (void);
// 0x0000007F System.Void RotarPisos::.ctor()
extern void RotarPisos__ctor_m1DE3AE8E6EAC635753F7BA8CCAB4C3D03243C3D0 (void);
// 0x00000080 System.Void RotarPuerta::girarPuerta()
extern void RotarPuerta_girarPuerta_mCD152178D8A9F8B8CFCE0EB56A1139F5C9447227 (void);
// 0x00000081 System.Void RotarPuerta::.ctor()
extern void RotarPuerta__ctor_mB61E0C1BC8838C52C0A21F00051FDDF10399D150 (void);
// 0x00000082 System.Void Sonido::.ctor()
extern void Sonido__ctor_m3CE32405ED71E91B6E0DB965D8E5F2A9748A9D5E (void);
// 0x00000083 System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x00000084 System.Void ControlJugador_<temporizador>d__62::.ctor(System.Int32)
extern void U3CtemporizadorU3Ed__62__ctor_m16F88DB1CFBACDF301EC209ACE11BB48B13CCCB3 (void);
// 0x00000085 System.Void ControlJugador_<temporizador>d__62::System.IDisposable.Dispose()
extern void U3CtemporizadorU3Ed__62_System_IDisposable_Dispose_mDD22C330ADFB8D60407B56E67A7B2964517F8450 (void);
// 0x00000086 System.Boolean ControlJugador_<temporizador>d__62::MoveNext()
extern void U3CtemporizadorU3Ed__62_MoveNext_m68AF8177A6EDBDF145044CCC2C7060F49D33052F (void);
// 0x00000087 System.Object ControlJugador_<temporizador>d__62::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtemporizadorU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F065C90792E0DC17050A9A74C747E96C04730E1 (void);
// 0x00000088 System.Void ControlJugador_<temporizador>d__62::System.Collections.IEnumerator.Reset()
extern void U3CtemporizadorU3Ed__62_System_Collections_IEnumerator_Reset_mBB0DD5803DAB29BDE7CAA87110DA86ADE35A6F37 (void);
// 0x00000089 System.Object ControlJugador_<temporizador>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CtemporizadorU3Ed__62_System_Collections_IEnumerator_get_Current_mE065E4A1594EB0D1FF1F9D694CBB6B3D34768FD9 (void);
// 0x0000008A System.Void GestorDeAudio_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m9EE3CF30A48339A97E7CD17BB38EBCF27E98976E (void);
// 0x0000008B System.Boolean GestorDeAudio_<>c__DisplayClass3_0::<ReproducirSonido>b__0(Sonido)
extern void U3CU3Ec__DisplayClass3_0_U3CReproducirSonidoU3Eb__0_m5AD897D4B351D1754570D3AD7C55B1DA655311BA (void);
// 0x0000008C System.Void GestorDeAudio_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mC523941BA702C9454871151D6A6C6CFB562DE6F8 (void);
// 0x0000008D System.Boolean GestorDeAudio_<>c__DisplayClass4_0::<PausarSonido>b__0(Sonido)
extern void U3CU3Ec__DisplayClass4_0_U3CPausarSonidoU3Eb__0_mBFB8CC79FDBC05C9CA1666AE3D249D54FAD3E8F4 (void);
// 0x0000008E System.Void DataPersistencia_Punto::.ctor(UnityEngine.Vector3)
extern void Punto__ctor_mEC2CFFF5F099E03D26E919757A75C676193E2E00 (void);
// 0x0000008F UnityEngine.Vector3 DataPersistencia_Punto::aVector()
extern void Punto_aVector_m1028659A6BD0799739502D145852A3D9CB1AABE6 (void);
// 0x00000090 System.Void Readme_Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
static Il2CppMethodPointer s_methodPointers[144] = 
{
	ControlCamara_Start_m3E41AB41B48C305B801CE88E1088E30F6B48065C,
	ControlCamara_Update_m739B8939B4C7E54D46E16102A9EBEE6DF983FFFD,
	ControlCamara__ctor_m954699EF3390352A462FE175B54EEB6A85882E34,
	ControlJugador_Awake_m177F022BE32DC1EFD64F026C1BB4A9782A40973C,
	ControlJugador_Start_m4E2BC1B8AEF94CFA13FA0A675ECC6CDD7C9E2546,
	ControlJugador_SetearTextos_m5E66A99531954232ACE79818FD29A2FC8B8580EF,
	ControlJugador_Update_m3C5ABBBBF954C1FB58E04A8E18F75DF313BE6FE8,
	ControlJugador_Pistola_mBEA2B0A854D01D23DE45AE64005A6F9058885896,
	ControlJugador_Escopeta_m8C0C0E0DF22DE82B213B0C23057789D6319C5362,
	ControlJugador_ArmaCuchillo_m70B5612A4481D7C92AA29A48D2AF572C82961751,
	ControlJugador_movimientoPersonaje_m2E9A20461C8AD1AD17973278E19CA9F679A92D92,
	ControlJugador_EstaEnPiso_m1FB74EA8781653CEF46B339AE1FF081E6FB370AB,
	ControlJugador_OnTriggerStay_mF42405D867CB49964E16A390ABB33B1972190E49,
	ControlJugador_OnCollisionEnter_m22C5CB05D3986EE56F0331609C2567921C385320,
	ControlJugador_OnCollisionStay_m09522BF80CEF5F04DC7254EC411EBA0C86C14A1B,
	ControlJugador_guadarDatos_m3B484AEC6CAE13C0C5896872BCF34EF9E2922D9D,
	ControlJugador_cargarDatos_mE0A6A798CDBE495550CC687385449FE7547AFDE4,
	ControlJugador_temporizador_m08B3483C82319E7AD928277333BA56640CCE7882,
	ControlJugador__ctor_m8081D7912A48835FE1D245E0CA7E6DD5D2E868CB,
	Corazon01_Start_m89C4976811E85BCB88137DD2B44909CA60048615,
	Corazon01_recibirDaUF1o_mF6C005DBCB5A3E5ADBBEE6654D9E5485BF00FE51,
	Corazon01_recibirDaUF1oEscopeta_m97F9C9AA604BECE174CA17DE028F1405EB23E032,
	Corazon01_recibirDaUF1oCuchillo_m3AC0622D7EC219F9CA5CE8DBAA60DBDA8C8AE068,
	Corazon01_OnTriggerEnter_mA637476AE5D3826732C8FADB0E4A82280A09B142,
	Corazon01__ctor_mA6C8BFA6907304BC4FC3234A17B4F21AF9E363A7,
	Corazon02_Start_m80034F1D8338D15BFDEE519B7C3D3BE39E787455,
	Corazon02_recibirDaUF1o_mE8FA32AF7F49358E7614B492EB6DA2E597B1D102,
	Corazon02_recibirDaUF1oEscopeta_m04A30A6701B9B857B448C3CBBD6532E574D9F113,
	Corazon02_recibirDaUF1oCuchillo_mE03C843CEEEB530A2198019BB4BDC3D6FF500721,
	Corazon02_OnTriggerEnter_mAD72DCF9C85560B7CB17E23A77B8E9C9F55871D4,
	Corazon02__ctor_m67AE92AB35B1A94331206ADAFC9D566E58623E95,
	Enemigo1_Start_mB31CAB00B80FC7C6CD74372A5C323FC0D67F9E6B,
	Enemigo1_Update_m33D0C5EF344DD7961D6A83CF83668F37BAD23853,
	Enemigo1_recibirDaUF1o_mCD1816865144CE7D8183A5BAF85ECA0477B0B195,
	Enemigo1_recibirDaUF1oEscopeta_m4D2C8884E0DA417B1E8369A977705E8B30F78DF2,
	Enemigo1_recibirDaUF1oCuchillo_m2BEA2A5560F2DAE696DD489399B1162F9716A4D2,
	Enemigo1_OnCollisionEnter_m03FFA061A7BE4CF07A7C8DEBE5111FBA1424C7F2,
	Enemigo1_desaparecer_m7E789D5ABEE57043EE924658F3EA63412EF155D9,
	Enemigo1__ctor_mA21518193FCA9FFB7EF696DEE06FB2E43D764BC0,
	Enemigo2_Awake_m6C46D97D7FB2BC697DA4FE42824324E6CF061ACA,
	Enemigo2_Start_m35BF6113BB1B50AC721AE2D6994B6D1966229687,
	Enemigo2_Update_m44F70814888E5DE05A90B7FB04546AACCAE6A5AB,
	Enemigo2_Subir_m30E108D7898FBBC24EF1A4AFB33D2F3E5AF1B538,
	Enemigo2_Bajar_mAFC69155485E00B1DD71DBD9E9E03B6309C66351,
	Enemigo2_recibirDaUF1o_m7A7B24E5557AA906BACC605298FDA52251822541,
	Enemigo2_recibirDaUF1oEscopeta_m65142986E766A9F54BAA3AB1A0BD32D872D7265E,
	Enemigo2_recibirDaUF1oCuchillo_m4D0E1CB1BAC0999EFBBB5904F893DFE9EC50AEBC,
	Enemigo2_OnCollisionEnter_m749039A88D721D9D967AAC772FEFA9A96E5D008D,
	Enemigo2__ctor_mC7860BFCA620127B4AD1E68CEDB4DED4A19F4C00,
	Enemigo3_Awake_m7C7D8E04AD17EAC0B0279DC643958D2DF517E37F,
	Enemigo3_Start_m813435C85BA38D5505D889870689516DDA40135B,
	Enemigo3_Update_m33C789BECD12FD0BF863525A3EB8EB976AEF3389,
	Enemigo3_Subir_mE6E761BCB428B20D3FEFFC0127AB8431D7DDF3E7,
	Enemigo3_Bajar_mF0A49A9D137E8904119082AC368BF24E7820492B,
	Enemigo3_recibirDaUF1o_mD9AA3DB7A86349D494455CBA42FA4C243B1C363D,
	Enemigo3_recibirDaUF1oEscopeta_mCE00839D3CF8D64CA51E969D3D1F8BE3190F4014,
	Enemigo3_recibirDaUF1oCuchillo_m626827ADB47A8ABADEF48837DA91A2E628B2B528,
	Enemigo3_OnCollisionEnter_mB8CA7CD60E8A54EA19EB7E591F70FA137B8C0A2C,
	Enemigo3__ctor_mDD8DB8EEAD78CE8E3D08C551655FD92CEF29A176,
	Enemigo3Max_Awake_m5458C526BFC6E1D76DC5D28705144A6AB838F6E8,
	Enemigo3Max_Start_m56EAB6E6F829D143EB3841430606454EB6B0AC1A,
	Enemigo3Max_Update_m4E1D7FF20154968B8E4416EA732582011E73138E,
	Enemigo3Max_Subir_m6CC92C48078685BFA4D57DFEDB571C8B569E5C74,
	Enemigo3Max_Bajar_mBA583AA518836066DA126E0BA143A7D542FDD407,
	Enemigo3Max_recibirDaUF1o_m9C66E08BDA1E02DC57D3B37DA1D603EA3C8FE891,
	Enemigo3Max_recibirDaUF1oEscopeta_mB7D5E97007BADDA99781F8C16992A19FA989480C,
	Enemigo3Max_recibirDaUF1oCuchillo_mBA390A4532206A889D81886C39A03030703878FD,
	Enemigo3Max_OnCollisionEnter_m5DFA7A9EB884786348F4A193F5CFFB550B84B17E,
	Enemigo3Max__ctor_m05D9F06810B5441ADC4AA06DC71A8F548CB2223A,
	Enemigo4_Start_m27F81CFCA504E9022AE6AF8876D9A004E17D5F59,
	Enemigo4_Update_m3708BAD12208B561D4AC601747D89D524366E96F,
	Enemigo4_Subir_m57164D61C86EADC333CB6D4803FD59322DAD0083,
	Enemigo4_Bajar_m72317D00F0F5CFE3692CE64F21659FE0B1EA89CA,
	Enemigo4_recibirDaUF1o_mE4B65374E232096AB3D3BF0E91232D71E3149AC9,
	Enemigo4_recibirDaUF1oEscopeta_m0D8EB29C9235471BB699E344551C6FFFD8E71228,
	Enemigo4_recibirDaUF1oCuchillo_m5E4C55A63E39770906EFA41CE9390A56058B0D1B,
	Enemigo4_OnCollisionEnter_m61885496DFFD3C98849D6D18192CD54827D3655A,
	Enemigo4__ctor_mA3F76054934D3C2BF41FBC125F16EF4AF4CEDA2C,
	Enemigo5_Start_m86ADB0305AA1834EBF228D5AFF07549496C43236,
	Enemigo5_Update_m7C523FD21C8373C521A20CB9AEE6AFD7E710F7EF,
	Enemigo5_recibirDaUF1o_m69B1CFEF055EBB96F33F744B42D4876B0D1A748E,
	Enemigo5_recibirDaUF1oEscopeta_mA0BBFF6347164C2CE7EF8156C1D8B44A8E0383BB,
	Enemigo5_recibirDaUF1oCuchillo_m69F1EA023C549BBAA8328F9381F3F53AC975E606,
	Enemigo5_OnCollisionEnter_m60E54D6A501AA9917B63F3F2D329BBC1B4676F54,
	Enemigo5__ctor_m0D27366C75DBC564093C8135A572D861FDAA9604,
	Enemigo6_Awake_m36AC7F1CD71CEE665745CAC6475FD4A89512824C,
	Enemigo6_Update_m3B43D18DA665CFDEFCAE0EF1F0C68AB0843BC163,
	Enemigo6_recibirDaUF1o_m98CDE7098974B4C0D1D6DBB12DEF793313181394,
	Enemigo6_OnCollisionEnter_m31D20DA9FA4A058547CB4D9FA6A3E78F05E7A442,
	Enemigo6__ctor_mE0458C87A3097E91513714789025333FF17993C2,
	GameOver_Start_mBEEA4334176BA871E2CD30E445D80AF8C0044AB1,
	GameOver_resgresarMainMenu_m7371FDE4F04392697FCA7A89BA663CDE29CD5453,
	GameOver__ctor_m4E80B334B510F1E21C8C80FE0DAE28A0058B5E3D,
	GestorDeAudio_Awake_m84CFA483ACF5BF5311A6ECE229C634A87EA65724,
	GestorDeAudio_ReproducirSonido_mAD4D45E1475BE5CD50434BA9A50F290DC25441C0,
	GestorDeAudio_PausarSonido_m1380B3D1103DB17218DB7288B04E857F3A329B1C,
	GestorDeAudio__ctor_mC1EBE323FE9A42E91A85736CA022B1D2161D64AC,
	GestorPersistencia_Awake_m018F88D5F06839EEF22D68C13C6EEE78F287B72C,
	GestorPersistencia_Update_m7FB4A1B54B1C87DC46947B5CED41754D3375D334,
	GestorPersistencia_GuardarDataPersistencia_m83BFFB9EAB6848F0A1918F79F07D2808857DCCE4,
	GestorPersistencia_CargarDataPersistencia_m8DE4A76A21E105ADDFAF8B1021EC18044A7CA991,
	GestorPersistencia__ctor_mB9BE796A49F0A1104A8A12E734002E157C8C08A7,
	DataPersistencia2__ctor_m93694CD4C749A07EB198AF448C84DBC0DED01E57,
	DataPersistencia__ctor_m674C6EBD140096D14760924D4479E74CB7BB08F5,
	MainMenu_EscenaJuego_m50BA49601483B40DD16EBB8C1C54EDE7953D5746,
	MainMenu_SalirJuego_m0C079DA9B577D1846D000D87C00A60593B83AD40,
	MainMenu__ctor_m63F945D965550BD614DCD2AE7F7489D4F28C5B30,
	MusicaFondo_Start_m8390F376955B1FEBFDFEBD9167BEA33E9EBFA002,
	MusicaFondo__ctor_m631372D66DACD2ED55A735DA535845725DC98007,
	Plataforma1_Update_mDBD0B17D103982652E6F0A891CDA6570A906308D,
	Plataforma1_Subir_m9CF73D2EBFC1835259E53B0F93F033C4D65069A7,
	Plataforma1_Bajar_m37270E0093D16C952E4CDDDA027533761BD71693,
	Plataforma1__ctor_mF92A649D1FCFBC0939A28B8CE7637E394DDEA243,
	Portal_OnTriggerEnter_mAD6086195F0290D3EB350FFCB0EABF6233B338D2,
	Portal__ctor_m6117B0C40EA244EFE65192DFF469E7C461EAC42F,
	ProteccionEnemigo5_Start_m1137A9C6FE1184EC9E6C313386151BDF72DFED0B,
	ProteccionEnemigo5_recibirDaUF1o_m937768DAE0E4D4C930097F60B6B49A680546A7D3,
	ProteccionEnemigo5_recibirDaUF1oEscopeta_m832E6D068EECC4D858752284F36B850A9F1FB63D,
	ProteccionEnemigo5_recibirDaUF1oCuchillo_mB474E273FF636395A2DACDF52DC0C851B4B691CA,
	ProteccionEnemigo5_OnTriggerEnter_mE96FD5382EE73DC0A88E33F33537FAAE6579CE21,
	ProteccionEnemigo5__ctor_m1DE2D8822072FB3B4734FD95346F68B240CD77B3,
	RayoAlienigena_Update_m48B6347E5A0E2F5FBBD33DEB7017DE291979E6C7,
	RayoAlienigena_Subir_m7CD54F979AA230426467D7CC6E4C0FA6C97E0025,
	RayoAlienigena_Bajar_m0FDD3918592B811B449B763B28D0611DB8116DD3,
	RayoAlienigena__ctor_m90D44D06F25A273B0BCD5D87E04CD70F4B791F20,
	RotarPisos_Update_m36A19CB92B0CC9DF80D2D3D81D878CCFF7927FFF,
	RotarPisos__ctor_m1DE3AE8E6EAC635753F7BA8CCAB4C3D03243C3D0,
	RotarPuerta_girarPuerta_mCD152178D8A9F8B8CFCE0EB56A1139F5C9447227,
	RotarPuerta__ctor_mB61E0C1BC8838C52C0A21F00051FDDF10399D150,
	Sonido__ctor_m3CE32405ED71E91B6E0DB965D8E5F2A9748A9D5E,
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	U3CtemporizadorU3Ed__62__ctor_m16F88DB1CFBACDF301EC209ACE11BB48B13CCCB3,
	U3CtemporizadorU3Ed__62_System_IDisposable_Dispose_mDD22C330ADFB8D60407B56E67A7B2964517F8450,
	U3CtemporizadorU3Ed__62_MoveNext_m68AF8177A6EDBDF145044CCC2C7060F49D33052F,
	U3CtemporizadorU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F065C90792E0DC17050A9A74C747E96C04730E1,
	U3CtemporizadorU3Ed__62_System_Collections_IEnumerator_Reset_mBB0DD5803DAB29BDE7CAA87110DA86ADE35A6F37,
	U3CtemporizadorU3Ed__62_System_Collections_IEnumerator_get_Current_mE065E4A1594EB0D1FF1F9D694CBB6B3D34768FD9,
	U3CU3Ec__DisplayClass3_0__ctor_m9EE3CF30A48339A97E7CD17BB38EBCF27E98976E,
	U3CU3Ec__DisplayClass3_0_U3CReproducirSonidoU3Eb__0_m5AD897D4B351D1754570D3AD7C55B1DA655311BA,
	U3CU3Ec__DisplayClass4_0__ctor_mC523941BA702C9454871151D6A6C6CFB562DE6F8,
	U3CU3Ec__DisplayClass4_0_U3CPausarSonidoU3Eb__0_mBFB8CC79FDBC05C9CA1666AE3D249D54FAD3E8F4,
	Punto__ctor_mEC2CFFF5F099E03D26E919757A75C676193E2E00,
	Punto_aVector_m1028659A6BD0799739502D145852A3D9CB1AABE6,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
};
static const int32_t s_InvokerIndices[144] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	26,
	26,
	26,
	23,
	23,
	1667,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	9,
	23,
	9,
	1131,
	1130,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	144,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
