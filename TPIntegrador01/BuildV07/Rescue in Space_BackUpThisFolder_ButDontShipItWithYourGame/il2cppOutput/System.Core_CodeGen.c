﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D (void);
// 0x00000006 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Reverse(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ReverseIterator(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000016 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Linq.ILookup`2<TKey,TSource> System.Linq.Enumerable::ToLookup(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000019 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000001A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000001B TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001C TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001D TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001E TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001F TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000020 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000021 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000022 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000023 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000025 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000027 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000028 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 (void);
// 0x00000029 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000002A System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88 (void);
// 0x0000002B System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000002C System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000002D TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000002E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000002F System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000030 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000031 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000035 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000036 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000037 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000039 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000003A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000003E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000003F System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000042 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000043 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000044 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000046 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000047 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000048 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000049 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000004A System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004D System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000004E System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000004F System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000051 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000052 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000053 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000054 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000057 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000058 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000059 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000005A TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000005B System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000005C System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000005D System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x0000005E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000005F System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000060 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000061 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000062 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000063 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000064 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000065 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000066 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000067 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000068 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x00000069 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000006A System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000006B System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000006C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000006D System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006E System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::.ctor(System.Int32)
// 0x0000006F System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.IDisposable.Dispose()
// 0x00000070 System.Boolean System.Linq.Enumerable_<ExceptIterator>d__77`1::MoveNext()
// 0x00000071 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::<>m__Finally1()
// 0x00000072 TSource System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000073 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.Reset()
// 0x00000074 System.Object System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.get_Current()
// 0x00000075 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000076 System.Collections.IEnumerator System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000077 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::.ctor(System.Int32)
// 0x00000078 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.IDisposable.Dispose()
// 0x00000079 System.Boolean System.Linq.Enumerable_<ReverseIterator>d__79`1::MoveNext()
// 0x0000007A TSource System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000007B System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.Reset()
// 0x0000007C System.Object System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.get_Current()
// 0x0000007D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000007E System.Collections.IEnumerator System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007F System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000080 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000081 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000082 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000083 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000084 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000085 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000086 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000087 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000088 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x00000089 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x0000008A System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x0000008B TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x0000008C System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000008D System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000008E System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x0000008F System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000090 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x00000091 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x00000092 System.Void System.Linq.Lookup`2::Resize()
// 0x00000093 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x00000094 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x00000095 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000096 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x00000097 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x00000098 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000099 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x0000009A System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x0000009B System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x0000009C System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x0000009D System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x0000009E System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x0000009F System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000A0 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000A1 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000A2 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x000000A3 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000A4 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000A5 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x000000A6 TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000A7 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000A8 System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000A9 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000AA System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000AB System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x000000AC System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000AD System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000AE System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000AF System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000B0 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000B1 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000B2 System.Void System.Linq.Set`1::Resize()
// 0x000000B3 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000B4 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000B5 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000B6 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B7 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000B8 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000B9 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BA System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000BB System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000BC System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000BD System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000BE TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000BF System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000C0 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000C1 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000C2 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000C3 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000C4 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000C5 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000C6 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000C7 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000C8 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000C9 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000CA System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000CB System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000CC TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000CD System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000CE System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000CF System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000D0 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000D2 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000D3 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000D4 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000D5 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000D6 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000D7 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000D8 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000D9 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000DA System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000DB System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000DC System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000DD System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000DE System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000DF System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000E0 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000E4 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000E6 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000E8 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000E9 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000EA System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000EB System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000EC System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000ED System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000EE System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000EF System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000F0 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000F1 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000F2 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[242] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[242] = 
{
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	95,
	-1,
	95,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[72] = 
{
	{ 0x02000004, { 110, 4 } },
	{ 0x02000005, { 114, 9 } },
	{ 0x02000006, { 125, 7 } },
	{ 0x02000007, { 134, 10 } },
	{ 0x02000008, { 146, 11 } },
	{ 0x02000009, { 160, 9 } },
	{ 0x0200000A, { 172, 12 } },
	{ 0x0200000B, { 187, 1 } },
	{ 0x0200000C, { 188, 2 } },
	{ 0x0200000D, { 190, 12 } },
	{ 0x0200000E, { 202, 11 } },
	{ 0x0200000F, { 213, 11 } },
	{ 0x02000010, { 224, 6 } },
	{ 0x02000011, { 230, 6 } },
	{ 0x02000012, { 236, 4 } },
	{ 0x02000013, { 240, 3 } },
	{ 0x02000017, { 243, 17 } },
	{ 0x02000018, { 264, 5 } },
	{ 0x02000019, { 269, 1 } },
	{ 0x0200001B, { 270, 8 } },
	{ 0x0200001D, { 278, 4 } },
	{ 0x0200001E, { 282, 3 } },
	{ 0x0200001F, { 285, 5 } },
	{ 0x02000020, { 290, 7 } },
	{ 0x02000021, { 297, 3 } },
	{ 0x02000022, { 300, 7 } },
	{ 0x02000023, { 307, 4 } },
	{ 0x02000024, { 311, 36 } },
	{ 0x02000026, { 347, 2 } },
	{ 0x06000006, { 0, 10 } },
	{ 0x06000007, { 10, 10 } },
	{ 0x06000008, { 20, 5 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 1 } },
	{ 0x0600000B, { 31, 2 } },
	{ 0x0600000C, { 33, 2 } },
	{ 0x0600000D, { 35, 4 } },
	{ 0x0600000E, { 39, 1 } },
	{ 0x0600000F, { 40, 2 } },
	{ 0x06000010, { 42, 1 } },
	{ 0x06000011, { 43, 2 } },
	{ 0x06000012, { 45, 1 } },
	{ 0x06000013, { 46, 2 } },
	{ 0x06000014, { 48, 1 } },
	{ 0x06000015, { 49, 5 } },
	{ 0x06000016, { 54, 3 } },
	{ 0x06000017, { 57, 2 } },
	{ 0x06000018, { 59, 4 } },
	{ 0x06000019, { 63, 2 } },
	{ 0x0600001A, { 65, 2 } },
	{ 0x0600001B, { 67, 4 } },
	{ 0x0600001C, { 71, 4 } },
	{ 0x0600001D, { 75, 3 } },
	{ 0x0600001E, { 78, 4 } },
	{ 0x0600001F, { 82, 4 } },
	{ 0x06000020, { 86, 3 } },
	{ 0x06000021, { 89, 3 } },
	{ 0x06000022, { 92, 1 } },
	{ 0x06000023, { 93, 3 } },
	{ 0x06000024, { 96, 2 } },
	{ 0x06000025, { 98, 3 } },
	{ 0x06000026, { 101, 2 } },
	{ 0x06000027, { 103, 5 } },
	{ 0x06000029, { 108, 1 } },
	{ 0x0600002B, { 109, 1 } },
	{ 0x0600003B, { 123, 2 } },
	{ 0x06000040, { 132, 2 } },
	{ 0x06000045, { 144, 2 } },
	{ 0x0600004B, { 157, 3 } },
	{ 0x06000050, { 169, 3 } },
	{ 0x06000055, { 184, 3 } },
	{ 0x0600008C, { 260, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[349] = 
{
	{ (Il2CppRGCTXDataType)2, 26149 },
	{ (Il2CppRGCTXDataType)3, 24492 },
	{ (Il2CppRGCTXDataType)2, 26150 },
	{ (Il2CppRGCTXDataType)2, 26151 },
	{ (Il2CppRGCTXDataType)3, 24493 },
	{ (Il2CppRGCTXDataType)2, 26152 },
	{ (Il2CppRGCTXDataType)2, 26153 },
	{ (Il2CppRGCTXDataType)3, 24494 },
	{ (Il2CppRGCTXDataType)2, 26154 },
	{ (Il2CppRGCTXDataType)3, 24495 },
	{ (Il2CppRGCTXDataType)2, 26155 },
	{ (Il2CppRGCTXDataType)3, 24496 },
	{ (Il2CppRGCTXDataType)2, 26156 },
	{ (Il2CppRGCTXDataType)2, 26157 },
	{ (Il2CppRGCTXDataType)3, 24497 },
	{ (Il2CppRGCTXDataType)2, 26158 },
	{ (Il2CppRGCTXDataType)2, 26159 },
	{ (Il2CppRGCTXDataType)3, 24498 },
	{ (Il2CppRGCTXDataType)2, 26160 },
	{ (Il2CppRGCTXDataType)3, 24499 },
	{ (Il2CppRGCTXDataType)2, 26161 },
	{ (Il2CppRGCTXDataType)3, 24500 },
	{ (Il2CppRGCTXDataType)3, 24501 },
	{ (Il2CppRGCTXDataType)2, 19562 },
	{ (Il2CppRGCTXDataType)3, 24502 },
	{ (Il2CppRGCTXDataType)2, 26162 },
	{ (Il2CppRGCTXDataType)3, 24503 },
	{ (Il2CppRGCTXDataType)3, 24504 },
	{ (Il2CppRGCTXDataType)2, 19569 },
	{ (Il2CppRGCTXDataType)3, 24505 },
	{ (Il2CppRGCTXDataType)3, 24506 },
	{ (Il2CppRGCTXDataType)2, 26163 },
	{ (Il2CppRGCTXDataType)3, 24507 },
	{ (Il2CppRGCTXDataType)2, 26164 },
	{ (Il2CppRGCTXDataType)3, 24508 },
	{ (Il2CppRGCTXDataType)3, 24509 },
	{ (Il2CppRGCTXDataType)2, 26165 },
	{ (Il2CppRGCTXDataType)2, 26166 },
	{ (Il2CppRGCTXDataType)3, 24510 },
	{ (Il2CppRGCTXDataType)3, 24511 },
	{ (Il2CppRGCTXDataType)2, 26167 },
	{ (Il2CppRGCTXDataType)3, 24512 },
	{ (Il2CppRGCTXDataType)3, 24513 },
	{ (Il2CppRGCTXDataType)2, 26168 },
	{ (Il2CppRGCTXDataType)3, 24514 },
	{ (Il2CppRGCTXDataType)3, 24515 },
	{ (Il2CppRGCTXDataType)2, 26169 },
	{ (Il2CppRGCTXDataType)3, 24516 },
	{ (Il2CppRGCTXDataType)3, 24517 },
	{ (Il2CppRGCTXDataType)3, 24518 },
	{ (Il2CppRGCTXDataType)2, 26170 },
	{ (Il2CppRGCTXDataType)2, 19607 },
	{ (Il2CppRGCTXDataType)2, 26171 },
	{ (Il2CppRGCTXDataType)2, 19609 },
	{ (Il2CppRGCTXDataType)2, 26172 },
	{ (Il2CppRGCTXDataType)3, 24519 },
	{ (Il2CppRGCTXDataType)3, 24520 },
	{ (Il2CppRGCTXDataType)2, 19615 },
	{ (Il2CppRGCTXDataType)3, 24521 },
	{ (Il2CppRGCTXDataType)3, 24522 },
	{ (Il2CppRGCTXDataType)2, 26173 },
	{ (Il2CppRGCTXDataType)3, 24523 },
	{ (Il2CppRGCTXDataType)2, 26174 },
	{ (Il2CppRGCTXDataType)2, 19621 },
	{ (Il2CppRGCTXDataType)3, 24524 },
	{ (Il2CppRGCTXDataType)2, 26175 },
	{ (Il2CppRGCTXDataType)3, 24525 },
	{ (Il2CppRGCTXDataType)2, 26176 },
	{ (Il2CppRGCTXDataType)2, 26177 },
	{ (Il2CppRGCTXDataType)2, 19625 },
	{ (Il2CppRGCTXDataType)2, 26178 },
	{ (Il2CppRGCTXDataType)2, 26179 },
	{ (Il2CppRGCTXDataType)2, 26180 },
	{ (Il2CppRGCTXDataType)2, 19627 },
	{ (Il2CppRGCTXDataType)2, 26181 },
	{ (Il2CppRGCTXDataType)2, 19629 },
	{ (Il2CppRGCTXDataType)2, 26182 },
	{ (Il2CppRGCTXDataType)3, 24526 },
	{ (Il2CppRGCTXDataType)2, 26183 },
	{ (Il2CppRGCTXDataType)2, 26184 },
	{ (Il2CppRGCTXDataType)2, 19632 },
	{ (Il2CppRGCTXDataType)2, 26185 },
	{ (Il2CppRGCTXDataType)2, 26186 },
	{ (Il2CppRGCTXDataType)2, 26187 },
	{ (Il2CppRGCTXDataType)2, 19634 },
	{ (Il2CppRGCTXDataType)2, 26188 },
	{ (Il2CppRGCTXDataType)2, 19636 },
	{ (Il2CppRGCTXDataType)2, 26189 },
	{ (Il2CppRGCTXDataType)3, 24527 },
	{ (Il2CppRGCTXDataType)2, 26190 },
	{ (Il2CppRGCTXDataType)2, 19639 },
	{ (Il2CppRGCTXDataType)2, 26191 },
	{ (Il2CppRGCTXDataType)2, 19641 },
	{ (Il2CppRGCTXDataType)2, 19643 },
	{ (Il2CppRGCTXDataType)2, 26192 },
	{ (Il2CppRGCTXDataType)3, 24528 },
	{ (Il2CppRGCTXDataType)2, 26193 },
	{ (Il2CppRGCTXDataType)2, 19646 },
	{ (Il2CppRGCTXDataType)2, 19648 },
	{ (Il2CppRGCTXDataType)2, 26194 },
	{ (Il2CppRGCTXDataType)3, 24529 },
	{ (Il2CppRGCTXDataType)2, 26195 },
	{ (Il2CppRGCTXDataType)3, 24530 },
	{ (Il2CppRGCTXDataType)3, 24531 },
	{ (Il2CppRGCTXDataType)2, 26196 },
	{ (Il2CppRGCTXDataType)2, 19653 },
	{ (Il2CppRGCTXDataType)2, 26197 },
	{ (Il2CppRGCTXDataType)2, 19655 },
	{ (Il2CppRGCTXDataType)3, 24532 },
	{ (Il2CppRGCTXDataType)3, 24533 },
	{ (Il2CppRGCTXDataType)3, 24534 },
	{ (Il2CppRGCTXDataType)3, 24535 },
	{ (Il2CppRGCTXDataType)2, 19664 },
	{ (Il2CppRGCTXDataType)3, 24536 },
	{ (Il2CppRGCTXDataType)3, 24537 },
	{ (Il2CppRGCTXDataType)2, 19676 },
	{ (Il2CppRGCTXDataType)2, 26198 },
	{ (Il2CppRGCTXDataType)3, 24538 },
	{ (Il2CppRGCTXDataType)3, 24539 },
	{ (Il2CppRGCTXDataType)2, 19678 },
	{ (Il2CppRGCTXDataType)2, 26009 },
	{ (Il2CppRGCTXDataType)3, 24540 },
	{ (Il2CppRGCTXDataType)3, 24541 },
	{ (Il2CppRGCTXDataType)2, 26199 },
	{ (Il2CppRGCTXDataType)3, 24542 },
	{ (Il2CppRGCTXDataType)3, 24543 },
	{ (Il2CppRGCTXDataType)2, 19688 },
	{ (Il2CppRGCTXDataType)2, 26200 },
	{ (Il2CppRGCTXDataType)3, 24544 },
	{ (Il2CppRGCTXDataType)3, 24545 },
	{ (Il2CppRGCTXDataType)3, 23262 },
	{ (Il2CppRGCTXDataType)3, 24546 },
	{ (Il2CppRGCTXDataType)2, 26201 },
	{ (Il2CppRGCTXDataType)3, 24547 },
	{ (Il2CppRGCTXDataType)3, 24548 },
	{ (Il2CppRGCTXDataType)2, 19700 },
	{ (Il2CppRGCTXDataType)2, 26202 },
	{ (Il2CppRGCTXDataType)3, 24549 },
	{ (Il2CppRGCTXDataType)3, 24550 },
	{ (Il2CppRGCTXDataType)3, 24551 },
	{ (Il2CppRGCTXDataType)3, 24552 },
	{ (Il2CppRGCTXDataType)3, 24553 },
	{ (Il2CppRGCTXDataType)3, 23268 },
	{ (Il2CppRGCTXDataType)3, 24554 },
	{ (Il2CppRGCTXDataType)2, 26203 },
	{ (Il2CppRGCTXDataType)3, 24555 },
	{ (Il2CppRGCTXDataType)3, 24556 },
	{ (Il2CppRGCTXDataType)2, 19713 },
	{ (Il2CppRGCTXDataType)2, 26204 },
	{ (Il2CppRGCTXDataType)3, 24557 },
	{ (Il2CppRGCTXDataType)3, 24558 },
	{ (Il2CppRGCTXDataType)2, 19715 },
	{ (Il2CppRGCTXDataType)2, 26205 },
	{ (Il2CppRGCTXDataType)3, 24559 },
	{ (Il2CppRGCTXDataType)3, 24560 },
	{ (Il2CppRGCTXDataType)2, 26206 },
	{ (Il2CppRGCTXDataType)3, 24561 },
	{ (Il2CppRGCTXDataType)3, 24562 },
	{ (Il2CppRGCTXDataType)2, 26207 },
	{ (Il2CppRGCTXDataType)3, 24563 },
	{ (Il2CppRGCTXDataType)3, 24564 },
	{ (Il2CppRGCTXDataType)2, 19730 },
	{ (Il2CppRGCTXDataType)2, 26208 },
	{ (Il2CppRGCTXDataType)3, 24565 },
	{ (Il2CppRGCTXDataType)3, 24566 },
	{ (Il2CppRGCTXDataType)3, 24567 },
	{ (Il2CppRGCTXDataType)3, 23279 },
	{ (Il2CppRGCTXDataType)2, 26209 },
	{ (Il2CppRGCTXDataType)3, 24568 },
	{ (Il2CppRGCTXDataType)3, 24569 },
	{ (Il2CppRGCTXDataType)2, 26210 },
	{ (Il2CppRGCTXDataType)3, 24570 },
	{ (Il2CppRGCTXDataType)3, 24571 },
	{ (Il2CppRGCTXDataType)2, 19746 },
	{ (Il2CppRGCTXDataType)2, 26211 },
	{ (Il2CppRGCTXDataType)3, 24572 },
	{ (Il2CppRGCTXDataType)3, 24573 },
	{ (Il2CppRGCTXDataType)3, 24574 },
	{ (Il2CppRGCTXDataType)3, 24575 },
	{ (Il2CppRGCTXDataType)3, 24576 },
	{ (Il2CppRGCTXDataType)3, 24577 },
	{ (Il2CppRGCTXDataType)3, 23285 },
	{ (Il2CppRGCTXDataType)2, 26212 },
	{ (Il2CppRGCTXDataType)3, 24578 },
	{ (Il2CppRGCTXDataType)3, 24579 },
	{ (Il2CppRGCTXDataType)2, 26213 },
	{ (Il2CppRGCTXDataType)3, 24580 },
	{ (Il2CppRGCTXDataType)3, 24581 },
	{ (Il2CppRGCTXDataType)3, 24582 },
	{ (Il2CppRGCTXDataType)3, 24583 },
	{ (Il2CppRGCTXDataType)3, 24584 },
	{ (Il2CppRGCTXDataType)3, 24585 },
	{ (Il2CppRGCTXDataType)2, 26214 },
	{ (Il2CppRGCTXDataType)2, 26215 },
	{ (Il2CppRGCTXDataType)3, 24586 },
	{ (Il2CppRGCTXDataType)2, 19781 },
	{ (Il2CppRGCTXDataType)2, 19775 },
	{ (Il2CppRGCTXDataType)3, 24587 },
	{ (Il2CppRGCTXDataType)2, 19774 },
	{ (Il2CppRGCTXDataType)2, 26216 },
	{ (Il2CppRGCTXDataType)3, 24588 },
	{ (Il2CppRGCTXDataType)3, 24589 },
	{ (Il2CppRGCTXDataType)3, 24590 },
	{ (Il2CppRGCTXDataType)2, 26217 },
	{ (Il2CppRGCTXDataType)3, 24591 },
	{ (Il2CppRGCTXDataType)2, 19797 },
	{ (Il2CppRGCTXDataType)2, 19789 },
	{ (Il2CppRGCTXDataType)3, 24592 },
	{ (Il2CppRGCTXDataType)3, 24593 },
	{ (Il2CppRGCTXDataType)2, 19788 },
	{ (Il2CppRGCTXDataType)2, 26218 },
	{ (Il2CppRGCTXDataType)3, 24594 },
	{ (Il2CppRGCTXDataType)3, 24595 },
	{ (Il2CppRGCTXDataType)3, 24596 },
	{ (Il2CppRGCTXDataType)2, 26219 },
	{ (Il2CppRGCTXDataType)3, 24597 },
	{ (Il2CppRGCTXDataType)2, 19810 },
	{ (Il2CppRGCTXDataType)2, 19802 },
	{ (Il2CppRGCTXDataType)3, 24598 },
	{ (Il2CppRGCTXDataType)3, 24599 },
	{ (Il2CppRGCTXDataType)2, 19801 },
	{ (Il2CppRGCTXDataType)2, 26220 },
	{ (Il2CppRGCTXDataType)3, 24600 },
	{ (Il2CppRGCTXDataType)3, 24601 },
	{ (Il2CppRGCTXDataType)2, 26221 },
	{ (Il2CppRGCTXDataType)3, 24602 },
	{ (Il2CppRGCTXDataType)2, 19814 },
	{ (Il2CppRGCTXDataType)2, 26222 },
	{ (Il2CppRGCTXDataType)3, 24603 },
	{ (Il2CppRGCTXDataType)3, 24604 },
	{ (Il2CppRGCTXDataType)3, 24605 },
	{ (Il2CppRGCTXDataType)2, 19824 },
	{ (Il2CppRGCTXDataType)3, 24606 },
	{ (Il2CppRGCTXDataType)2, 26223 },
	{ (Il2CppRGCTXDataType)3, 24607 },
	{ (Il2CppRGCTXDataType)3, 24608 },
	{ (Il2CppRGCTXDataType)2, 26224 },
	{ (Il2CppRGCTXDataType)3, 24609 },
	{ (Il2CppRGCTXDataType)2, 19832 },
	{ (Il2CppRGCTXDataType)3, 24610 },
	{ (Il2CppRGCTXDataType)2, 26225 },
	{ (Il2CppRGCTXDataType)3, 24611 },
	{ (Il2CppRGCTXDataType)2, 26225 },
	{ (Il2CppRGCTXDataType)2, 19862 },
	{ (Il2CppRGCTXDataType)3, 24612 },
	{ (Il2CppRGCTXDataType)3, 24613 },
	{ (Il2CppRGCTXDataType)3, 24614 },
	{ (Il2CppRGCTXDataType)3, 24615 },
	{ (Il2CppRGCTXDataType)2, 26226 },
	{ (Il2CppRGCTXDataType)2, 26227 },
	{ (Il2CppRGCTXDataType)2, 26228 },
	{ (Il2CppRGCTXDataType)3, 24616 },
	{ (Il2CppRGCTXDataType)3, 24617 },
	{ (Il2CppRGCTXDataType)2, 19858 },
	{ (Il2CppRGCTXDataType)2, 19861 },
	{ (Il2CppRGCTXDataType)3, 24618 },
	{ (Il2CppRGCTXDataType)3, 24619 },
	{ (Il2CppRGCTXDataType)2, 19865 },
	{ (Il2CppRGCTXDataType)3, 24620 },
	{ (Il2CppRGCTXDataType)2, 26229 },
	{ (Il2CppRGCTXDataType)2, 19855 },
	{ (Il2CppRGCTXDataType)2, 26230 },
	{ (Il2CppRGCTXDataType)3, 24621 },
	{ (Il2CppRGCTXDataType)3, 24622 },
	{ (Il2CppRGCTXDataType)3, 24623 },
	{ (Il2CppRGCTXDataType)2, 26231 },
	{ (Il2CppRGCTXDataType)3, 24624 },
	{ (Il2CppRGCTXDataType)3, 24625 },
	{ (Il2CppRGCTXDataType)3, 24626 },
	{ (Il2CppRGCTXDataType)2, 19880 },
	{ (Il2CppRGCTXDataType)3, 24627 },
	{ (Il2CppRGCTXDataType)2, 26232 },
	{ (Il2CppRGCTXDataType)2, 26233 },
	{ (Il2CppRGCTXDataType)3, 24628 },
	{ (Il2CppRGCTXDataType)3, 24629 },
	{ (Il2CppRGCTXDataType)2, 19902 },
	{ (Il2CppRGCTXDataType)3, 24630 },
	{ (Il2CppRGCTXDataType)2, 19903 },
	{ (Il2CppRGCTXDataType)3, 24631 },
	{ (Il2CppRGCTXDataType)2, 26234 },
	{ (Il2CppRGCTXDataType)3, 24632 },
	{ (Il2CppRGCTXDataType)3, 24633 },
	{ (Il2CppRGCTXDataType)2, 26235 },
	{ (Il2CppRGCTXDataType)3, 24634 },
	{ (Il2CppRGCTXDataType)3, 24635 },
	{ (Il2CppRGCTXDataType)2, 26236 },
	{ (Il2CppRGCTXDataType)3, 24636 },
	{ (Il2CppRGCTXDataType)3, 24637 },
	{ (Il2CppRGCTXDataType)3, 24638 },
	{ (Il2CppRGCTXDataType)2, 19934 },
	{ (Il2CppRGCTXDataType)3, 24639 },
	{ (Il2CppRGCTXDataType)2, 19943 },
	{ (Il2CppRGCTXDataType)3, 24640 },
	{ (Il2CppRGCTXDataType)2, 26237 },
	{ (Il2CppRGCTXDataType)2, 26238 },
	{ (Il2CppRGCTXDataType)3, 24641 },
	{ (Il2CppRGCTXDataType)3, 24642 },
	{ (Il2CppRGCTXDataType)3, 24643 },
	{ (Il2CppRGCTXDataType)3, 24644 },
	{ (Il2CppRGCTXDataType)3, 24645 },
	{ (Il2CppRGCTXDataType)3, 24646 },
	{ (Il2CppRGCTXDataType)2, 19959 },
	{ (Il2CppRGCTXDataType)2, 26239 },
	{ (Il2CppRGCTXDataType)3, 24647 },
	{ (Il2CppRGCTXDataType)3, 24648 },
	{ (Il2CppRGCTXDataType)2, 19963 },
	{ (Il2CppRGCTXDataType)3, 24649 },
	{ (Il2CppRGCTXDataType)2, 26240 },
	{ (Il2CppRGCTXDataType)2, 19973 },
	{ (Il2CppRGCTXDataType)2, 19971 },
	{ (Il2CppRGCTXDataType)2, 26241 },
	{ (Il2CppRGCTXDataType)3, 24650 },
	{ (Il2CppRGCTXDataType)2, 26242 },
	{ (Il2CppRGCTXDataType)3, 24651 },
	{ (Il2CppRGCTXDataType)3, 24652 },
	{ (Il2CppRGCTXDataType)2, 19980 },
	{ (Il2CppRGCTXDataType)3, 24653 },
	{ (Il2CppRGCTXDataType)2, 19980 },
	{ (Il2CppRGCTXDataType)3, 24654 },
	{ (Il2CppRGCTXDataType)2, 19997 },
	{ (Il2CppRGCTXDataType)3, 24655 },
	{ (Il2CppRGCTXDataType)3, 24656 },
	{ (Il2CppRGCTXDataType)3, 24657 },
	{ (Il2CppRGCTXDataType)2, 26243 },
	{ (Il2CppRGCTXDataType)3, 24658 },
	{ (Il2CppRGCTXDataType)3, 24659 },
	{ (Il2CppRGCTXDataType)3, 24660 },
	{ (Il2CppRGCTXDataType)2, 19977 },
	{ (Il2CppRGCTXDataType)3, 24661 },
	{ (Il2CppRGCTXDataType)3, 24662 },
	{ (Il2CppRGCTXDataType)2, 19982 },
	{ (Il2CppRGCTXDataType)3, 24663 },
	{ (Il2CppRGCTXDataType)1, 26244 },
	{ (Il2CppRGCTXDataType)2, 19981 },
	{ (Il2CppRGCTXDataType)3, 24664 },
	{ (Il2CppRGCTXDataType)1, 19981 },
	{ (Il2CppRGCTXDataType)1, 19977 },
	{ (Il2CppRGCTXDataType)2, 26243 },
	{ (Il2CppRGCTXDataType)2, 19981 },
	{ (Il2CppRGCTXDataType)2, 19979 },
	{ (Il2CppRGCTXDataType)2, 19983 },
	{ (Il2CppRGCTXDataType)3, 24665 },
	{ (Il2CppRGCTXDataType)3, 24666 },
	{ (Il2CppRGCTXDataType)3, 24667 },
	{ (Il2CppRGCTXDataType)3, 24668 },
	{ (Il2CppRGCTXDataType)3, 24669 },
	{ (Il2CppRGCTXDataType)2, 19978 },
	{ (Il2CppRGCTXDataType)3, 24670 },
	{ (Il2CppRGCTXDataType)2, 19993 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	242,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	72,
	s_rgctxIndices,
	349,
	s_rgctxValues,
	NULL,
};
